
var exports = window.SketchVector = {};

(function(exports){
    exports.scale = function(result, s,a){
        if(!result) result = [0,0];
        result[0] = s*a[0];
        result[1] = s*a[1];
        return result; }

    exports.add = function(result, a,b){
        if(!result) result = [0,0];
        result[0] = a[0] + b[0];
        result[1] = a[1] + b[1];
        return result; }
        
    exports.minus = function(result, a,b){
        if(!result) result = [0,0];
        result[0] = a[0] - b[0];
        result[1] = a[1] - b[1];
        return result; }

    exports.dist = function(a,b){
        var dx = a[0] - b[0],
            dy = a[1] - b[1];
        return Math.sqrt(dx*dx + dy*dy); },

    exports.dot = function(a,b){
        return a[0]*b[0] + a[1]*b[1]; },

    exports.norm = function(a){
        return Math.sqrt(a[0]*a[0] + a[1]*a[1]); },

    exports.equal = function(a,b, margin){
        if(!margin) margin = 0.0000001;
        if(Math.abs(a[0]-b[0])<margin && Math.abs(a[1]-b[1])<margin){
            return true;}
        return false;
    }
    exports.assign = function(result, a){
        if(!result) result = [0,0];
        result[0] = a[0];
        result[1] = a[1];
        return result; }

})(exports);
