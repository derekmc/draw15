
var exports = window.SketchGeometry = {};

(function(exports){

exports.screenToCanvas = screenToCanvas;
exports.canvasToScreen = canvasToScreen;
exports.screenZoomedToCanvas = screenZoomedToCanvas;
exports.canvasToScreenZoomed = canvasToScreenZoomed;
exports.applyToolWidthLimits = applyToolWidthLimits;
exports.zoomWindowBounds = zoomWindowBounds;
exports.fitZoomBox = fitZoomBox;
exports.sceneWindowDisplayFocus = sceneWindowDisplayFocus;

// given the ui, and the bounds of scene window, find the display focus
function sceneWindowDisplayFocus(ui, bounds){
    var w = ui.canvas.width;
    var h = ui.canvas.height;
    var x0 = bounds[0];
    var y0 = bounds[1];
    var boxw = bounds[2] - x0;
    var boxh = bounds[3] - y0;
    var x = x0 + boxw / 2;
    var y = y0 + boxh / 2;
    return [x, y];
}

function screenToCanvas(result, state,pos){
     return state.scene_display.displayToSpace(result, pos);
}
function canvasToScreen(result, state,pos){
     return state.scene_display.spaceToDisplay(result, pos);
}

function screenZoomedToCanvas(result, state,pos){
     return state.zoom_display.displayToSpace(result, pos);
}

function canvasToScreenZoomed(result, state, pos){
     return state.zoom_display.spaceToDisplay(result, pos);
}

// based on the zoom level, there are minimum and maximum tool widths,
// use this only when the tool is actually used.
function applyToolWidthLimits(state){
    var zoomlevel = state.show_scene? state.scene_display.getScale() : state.zoom_display.getScale();
    if(zoomlevel < 1) zoomlevel = 1;
    var max = state.tool_width_max / zoomlevel;
    var min = 1 / zoomlevel;
    var changed = false;
    if(state.erase){
        if(state.remove_width > max){
            state.remove_width = max;
            changed = true; }
        if(state.remove_width < min){
            state.remove_width = min;
            changed = true; }}
    else{
        var width = state.pen_width;
        if(width > max){
            state.pen_width = max;
            changed = true; }
        if(width < min){
            state.pen_width = min; 
            changed = true; }}
    return changed;
}

function zoomWindowBounds(state){
    var w = state.width;
    var h = state.height;
    var d1 = state.zoom_display;
    var d2 = state.scene_display;
    var a = [0,0,0,0];
    d2.spaceToDisplay(a, d1.displayToSpace(a, a));
    var b = [w,h];
    d2.spaceToDisplay(b, d1.displayToSpace(b, b));
    a[2] = b[0];
    a[3] = b[1];
    return  a;
}

function fitZoomBox(state, dontpan){
    // only perform fit when show_scene is enabled.
    if(!state.show_scene){
        return; }
    var w = state.width;
    var h = state.height;
    var zoom = state.zoom_display.getScale();
    var zoom0 = state.scene_display.getScale();
    var zoom_factor = zoom/zoom0;

    //if(zoom_factor < 1) zoom_factor = 1;

    var boxw = w/zoom_factor;
    var boxh = h/zoom_factor;
    var d1 = state.zoom_display;
    var d2 = state.scene_display;
    var zoom_window = zoomWindowBounds(state);
    var x0 = zoom_window[0];
    var y0 = zoom_window[1];
    var x1 = zoom_window[2];
    var y1 = zoom_window[3];
    var z1 = d1.getScale();
    var z2 = d2.getScale();


    dontpan = true;
    if(x0 < 0){
        if(dontpan){
            d1.moveSpaceFocus(-x0/z1, 0); }
        else{
            d2.moveSpaceFocus(x0/z2, 0); }}
    if(y0 < 0){
        if(dontpan){
            d1.moveSpaceFocus(0, -y0/z1); }
        else{
            d2.moveSpaceFocus(0, y0/z2); }}
    if(x1 > w){
        if(dontpan){
            d1.moveSpaceFocus((w-x1)/z1, 0); }
        else{
            d2.moveSpaceFocus((x1-w)/z2, 0); }}
    if(y1 > h){
        if(dontpan){
            d1.moveSpaceFocus(0, (h-y1)/z1); }
        else{
            d2.moveSpaceFocus(0, (y1-h)/z2); }}
    /*
    var zoom_window = zoomWindowBounds(state);
    var display_focus = SketchGeometry.sceneWindowDisplayFocus(ui, scene_window);
    var space_focus = zoomed.displayToSpace(null, display_focus);
    scene.setDisplayFocus(display_focus);
    //zoomed.setSpaceFocus(space_focus);
    //scene.setSpaceFocus(space_focus);
    state.scene_display_focus_ratio[0] = display_focus[0]/w;
    state.scene_display_focus_ratio[1] = display_focus[1]/h;
    */
}
// 2 space vector functions.
function add2(result, v1, v2){
    if(!result) result = [0,0];
    result[0] = v1[0] + v2[0];
    result[1] = v1[1] + v2[1];
    return result;
}
function sub2(result, v1, v2){
    if(!result) result = [0,0];
    result[0] = v1[0] - v2[0];
    result[1] = v1[1] - v2[1];
    return result;
}
function dot2(v1, v2){
    return v1[0]*v2[0] + v1[1]*v2[1];
}
function scale2(result, s, v){
    if(!result) result = [0,0];
    result[0] = s * v[0];
    result[1] = s * v[1];
    return result;
}
function dist2(v1, v2){
    var dx = v2[0] - v1[0];
    var dy = v2[1] - v1[1];
    return Math.sqrt(dx*dx + dy*dy);
}
})(exports);
