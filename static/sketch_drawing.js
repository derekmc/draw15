
var exports = window.SketchDrawing = {};


(function(exports){

exports.SketchDrawing = SketchDrawing;


function SketchDrawing(){
    if(!(this instanceof SketchDrawing)){
        throw "SketchDrawing must be called as constructor with 'new' keyword"; }

    // the point array is the seqence of point inputs.
    // It is stored in an Int32Array which is only expanded and reallocated when filled.
    // strokes reference subsequences of the input seqence.
    // TODO hide, make private.
    this.points_size = 0x400;
    this.point_index = 0;
    this.point_grain = 0x40;  // 1/(size of pixel)
    this.points = new Int32Array(0x400); // [x0,y0, x1,y1, ...]
    // TODO parameterized ranges [t_n, t_n+1], used to draw only segments of this point.
    // the integer part is the stroke point index,
    // the decimal part is where it starts between this point and the next point
    // strokes can have several subranges.
    // TODO special undo entry for segmenting strokes like this.
    // TODO find intersection of 2 lines, for segmenting

    // cut_stroke: [width, x0, y0, x1, y1, ...]
    // TODO drawing.applyCutStroke(stroke, cut_stroke1, ....)
    this.strokes = []; // [[style, width, point_index0, point_index1, t0, t1...] ...] 
    // t1 and t0 indicate the parameterized start or end of beginning or ending subsegments.

    // memoize
    var stroke_bounds = {};

    this.clear = function(){
        this.points_size = 0x400;
        this.points = new Int32Array(0x400);
        this.strokes = [];
        this.point_index = 0;
    }
    this.numToGrain = function(x){
        return Math.floor(x * this.point_grain);
    }
    this.grainToNum = function(n){
        return (0.0 + n)/this.point_grain;
    }
    this.addPoint = function(x,y){
        x = this.numToGrain(x);
        y = this.numToGrain(y);
        if(2*this.point_index + 2 > this.points_size){
             this.expandPoints(); }
        ++this.point_index;
        this.points[2*this.point_index - 2] = x;
        this.points[2*this.point_index - 1] = y;
    }
    this.changeLastPoint = function(x,y){  //used by lineTool
        x = this.numToGrain(x);
        y = this.numToGrain(y);
        var i = 2*this.point_index;
        if(this.point_index < 1) return false;
        if(this.points[i-2] == x && this.points[i-1] == y){
            return false; } // unchanged
        this.points[i-2] = x;
        this.points[i-1] = y;
        return true;
    }
    this.getPoint = function(i){
        if(i<0){ // pythonic negative indexes.
            i += this.point_index; }
        if(i<0 || i>=this.point_index){
            return null; }
        return [
            this.grainToNum(this.points[2*i]), 
            this.grainToNum(this.points[2*i+1])];
    }
    this.getLastPoint = function(){
        return getPoint(this.point_index - 1);
    }
    this.expandPoints = function(){
        var new_size = Math.floor(3*this.points_size/2);
        var new_array = new Int32Array(new_size);
        for(var i=0; i<2*this.point_index; ++i){
            new_array[i] = this.points[i]; }
        this.points_size = new_size;
        this.points = new_array;
    }
    // for these stroke functions, the stroke is provided as a parameter,
    // while everything else comes from the object instance.
    // that may be inconsistent, but it makes sense with how it is used in context.
    this.strokeFirstPoint = function(pt, stroke){
        return this.strokePoint(pt, stroke, 0);
    }
    this.strokeLastPoint = function(pt, stroke){
        return this.strokePoint(pt, stroke, stroke[3]-1);
    }
    this.strokeBounds = function(result_buffer, stroke_index){
        if(stroke_bounds.hasOwnProperty(stroke_index)){
            var bounds = stroke_bounds[stroke_index];
            if(result_buffer == null){
                return bounds; }
            result_buffer[0] = bounds[0];
            result_buffer[1] = bounds[1];
            result_buffer[2] = bounds[2];
            result_buffer[3] = bounds[3];
            return result_buffer; }
        if(result_buffer == null) result_buffer = [0,0,0,0];
        var minx=null, maxx=0, miny=0, maxy=0;
        var stroke = this.strokes[stroke_index];
        var point_count = stroke[3] - stroke[2] + 1;

        for(var i=0; i<point_count; ++i){
            // reuse result_buffer
            this.strokePoint(result_buffer, stroke, i);
            var x = result_buffer[0], y = result_buffer[1];
            if(i == 0){
                minx = x;
                miny = y;
                maxx = x;
                maxy = y; }
            else{
                if(x < minx) minx = x;
                if(y < miny) miny = y;
                if(x > maxx) maxx = x;
                if(y > maxy) maxy = y; }}
        result_buffer[0] = minx;
        result_buffer[1] = miny;
        result_buffer[2] = maxx;
        result_buffer[3] = maxy;
        stroke_bounds[stroke_index] = result_buffer.slice(0);
        return result_buffer;

    }

    // I wonder, is it ever worth it to not use this function,
    // and just do the computation directly in context,
    // to save the parameterization checks and function overhead?
    this.strokePoint = function(pt, stroke, index){
        var index0 = 2*stroke[2];
        var index1 = 2*stroke[3];
        var array = this.points;
        var grain = this.point_grain;
        var n = index1 - index0;
        if(pt == null || pt == undefined){
            pt = [0,0]; }
        if(stroke.length < 4){
            throw "SketchDrawing: stroke should have at least 4 entries: " + JSON.stringify(stroke); }
        index *= 2;
        index += index0;
        pt[0] = array[index]/grain;
        pt[1] = array[index+1]/grain;

        if(index == index0 && stroke.length >= 5){
            // parameterized start point
            var t = stroke[4];
            pt[0] *= 1 - t;
            pt[1] *= 1 - t;
            pt[0] += t * array[index+2]/grain;
            pt[1] += t * array[index+3]/grain;
        }
        else if(index == index1 && stroke.length >= 6){
            // parameterized end point
            var t = stroke[5];
            //t = 0.5;
            pt[0] *= t;
            pt[1] *= t;
            pt[0] += (1-t) * array[index-2]/grain;
            pt[1] += (1-t) * array[index-1]/grain;
        }

        return pt;
    }
}


})(exports);
