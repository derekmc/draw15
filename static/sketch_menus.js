
var exports = this.SketchMenu = {};
// main menu for ghost sketch
(function(exports){

var m = LispMarkup.macros;

var menus = {};
// click actions are in menu_main.js
//*
var version = localStorage.getItem("installed_version");
var not_installed = false;
var new_version = false;
var online = (typeof AVAILABLE_VERSION != 'undefined');
if(version === null || version === undefined){
    not_installed = true; }
if(version != "uninstalled" && online && version != AVAILABLE_VERSION){
    new_version = true; }
var enable_file_menu = true;
    

function scene_menu_text(state){
    return "Scene " + (state.show_scene? "On" : "Off");
    //return "Scene " + (state.show_scene? "Off" : (state.scene_ratios.length == 1? "On" : "x" + state.scene_ratio));
}

menus.main = ['.menu', {onclick: "menuclick(event,'back')"},
  ['#overlay_background'],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['#less_spacer', 'spacer'],
  (!enable_file_menu? "" :
    ['.column',
      ['h2', 'File', {onclick: "menuclick(event,'navigate','file')"}]]),
  ['.column',
    ['h2', 'Color', {onclick: "menuclick(event,'navigate','color')"}]],
  ['.column',
    ['h2', 'Tool', {onclick: "menuclick(event,'navigate','tool')"}]],
  ['.column',
    ['h2'+(new_version?".update_available":""), (new_version?"*":"") + "App", {onclick: "menuclick(event,'navigate','app')"}]],
];

menus.file = ['.menu', {onclick: "menuclick(event,'back')"},
  ['#overlay_background', {onclick: "menuclick(event,'back')"}],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['#less_spacer', 'spacer'],
  ['.column',
    ['h2', 'File', {onclick: "menuclick(event,'back')"}],
    ['.entry', 'Save', {onclick: "menuclick(event,'save')"}],
    ['.entry', 'Load', {onclick: "menuclick(event,'load')"}],
    ['.entry', 'View', {onclick: "menuclick(event,'page_view')"}],
    ['.entry', 'Clear', {onclick: "menuclick(event,'page_clear')"}]],
  ['.column',
    ['h2', 'Color', {onclick: "menuclick(event,'backnav','color')"}]],
  ['.column',
    ['h2', 'Tool', {onclick: "menuclick(event,'backnav','tool')"}]],
  ['.column',
    ['h2'+(new_version?".update_available":""), (new_version?"*":"") + 'App', {onclick: "menuclick(event,'backnav','app')"}]],
];

menus.color = ['.menu', {onclick: "menuclick(event,'back')"},
  ['#overlay_background'],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['#less_spacer', 'spacer'],
  (!enable_file_menu? "" :
    ['.column',
      ['h2', 'File', {onclick: "menuclick(event,'backnav','file')"}]]),
  ['.column',
    ['h2', 'Color', {onclick: "menuclick(event,'back')"}],
    ['.entry', "#", function(state){ return "" + (state.pen_index+1); },
               {onclick: "menuclick(event, 'pen_index')", style: function(state){
                   var bg = SketchColor.getStateColorOpaque(state, state.pen_index, true);
                   return "background: " + bg + "; " + 
                          "color: white;" +
                          "text-stroke: 1px black;" + 
                          "text-shadow: #000 0px 0px 1px;" + 
                          "border: 2px solid black;" }}],
    ['.entry', 'Hex Color', {onclick: "menuclick(event,'pen_color')"}],
    ['.entry', 'Brightness', {onclick: "menuclick(event, 'pen_brightness')"}]],
  ['.column',
    ['h2', 'Tool', {onclick: "menuclick(event,'backnav','tool')"}]],
  ['.column',
    ['h2'+(new_version?".update_available":""), (new_version?"*":"") + "App", {onclick: "menuclick(event,'backnav','app')"}]],
];

menus.tool = ['.menu', {onclick: "menuclick(event,'back')"},
  ['#overlay_background'],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['#less_spacer', 'spacer'],
  (!enable_file_menu? "" :
    ['.column',
      ['h2', 'File', {onclick: "menuclick(event,'backnav','file')"}]]),
  ['.column',
    ['h2', 'Color', {onclick: "menuclick(event,'backnav','color')"}]],
  ['.column',
    ['h2', "Tool", { onclick:"menuclick(event, 'back')"}],
    ['.entry',
      ["IF", "pencil_enabled", "Pencil", "Pen"],
      {onclick: "menuclick(event,'toggle_pencil')"}],
    ['.entry', ["IF", "draw_line_enabled", "Line", "Curve"],
      {onclick: "menuclick(event,'pen_drawline')"}],
    ['.entry', 'Pencil Opacity', {onclick: "menuclick(event,'pencil_opacity')"}],
    ['.entry',
      {"class": [m.IF, "drawfore", "entry foreground", "entry background"]},
      {"style": "border-radius: 0; text-transform: lowercase;"},
      [m.IF, "drawfore", "Foreground", "Background"],
      {onclick: "menuclick(event,'pen_drawfore')"}],
    ['.entry', scene_menu_text, {onclick: "menuclick(event,'next_scene_setting')"}]],
    //['.entry', [m.IF, [m.GET, "is_erase_tool"], "Erase", "Remove"],
      //{onclick: "menuclick(event,'tool_erasetoggle')"}]]
  ['.column',
    ['h2'+(new_version?".update_available":""), (new_version?"*":"") + 'App', {onclick: "menuclick(event,'backnav','app')"}]],
]

menus.app = ['.menu', {onclick: "menuclick(event,'back')"},
  ['#overlay_background'],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['#less_spacer', 'spacer'],
  (!enable_file_menu? "" :
    ['.column',
      ['h2', 'File', {onclick: "menuclick(event,'backnav','file')"}]]),
  ['.column',
    ['h2', 'Color', {onclick: "menuclick(event,'backnav','color')"}]],
  ['.column',
    ['h2', "Tool", {onclick:"menuclick(event,'backnav','tool')"}]],
  ['.column',
    ['h2', "App", {onclick: "menuclick(event,'back')"}],
    ['.entry', 'Help', {onclick: "menuclick(event,'browser_navigate','help.html');"}],
    (function(){ return !online? "" : ['.entry'+(new_version?".update_available":""),
      (new_version? (not_installed? "Install" : "Update") : "Version"),
      {onclick: "menuclick(event,'browser_navigate','../update/');"}]; })()],
    //['.entry.disabled', 'About', {onclick: "menuclick(event,'app_about');"}]
]


/*
menus.color = ['.menu',
  ['#overlay_background', {onclick: "menuclick(event,'back')"}],
  ['#less.back', 'BACK'],
  ['#more', 'v'],
  ['h1', 'Colors'],
  ['.column',
    ['h2', '2/7'],
    ['.entry', ['input', {type: 'color', value: '#ff0000'}]], 
    ['.entry', 'Next', {onclick: "menuclick(event,'color_next');"}],
    ['.entry', 'Prev', {onclick: "menuclick(event,'color_prev');"}],
    ['.entry', 'New', {onclick: "menuclick(event,'color_new');"}],
    ['.entry', 'Delete', {onclick: "menuclick(event,'color_delete');"}]],
]
/**/

menus.confirm = [
  ['.modal', 
    ['.modal_inner',
      [m.GET, 'message', 'Are You Sure?'],
      ['div', {style:"display: inline-block;"},
        ['button', 
          [m.GET, 'yes_option', 'Yes'],
          {onclick: "menuclick(event,'confirm_yes');"},],
        ['button',
          [m.GET, 'no_option', 'No'],
          {onclick: "menuclick(event,'confirm_no');"}]]]]]

menus.alert = [
  ['.modal',
    ['#overlay_background', {onclick: "menuclick(event,'back')"}],
    ['.modal_inner',
      [m.GET, 'message', 'Alert'],
      ['button', 'Ok', {onclick: "menuclick(event,'back')"}]]]];

menus.choose_file = [
  ['.modal',
    ['.modal_inner',
      [m.GET, 'message', ''],
      ['div', {style: "display: inline-block;"}, ' ',
       ['input#prompt_input', {type: 'file', onchange:"menuclick(event,'prompt_done')"}], ' ',
       ['button', 'Cancel', {onclick: "menuclick(event,'back')"}]]]]]


    
menus.prompt = [
  ['.modal',
    ['.modal_inner',
      [m.GET, 'message', ''],
      ['div', {style: "display: block;"}, ' ',
       ['input#prompt_input']],' ',
      ['div', {style: "display: block;"}, ' ',
       ['button', 'Done', {onclick: "menuclick(event,'prompt_done')"}],
       ['button', 'Cancel', {onclick: "menuclick(event,'back')"}]]]]]

/*var embedpage_header = LispMarkup.toHtml(['.menu',
  ['.back', 'Back', {onclick: "menuclick(event,'back')"}],
]);*/

function setClass(elem, classname,value){
    if(value){
        elem.classList.add(classname); }
    else{
        elem.classList.remove(classname); }}

function seekLess(event){
    event.stopPropagation();
    document.body.scrollTop -= 0.5 * window.innerHeight; }
function seekMore(event){
    event.stopPropagation();
    document.body.scrollTop += 0.5 * window.innerHeight; }

function refreshSeekButtons(){
    var less = document.getElementById('less');
    var more = document.getElementById('more');
    var overlay = document.getElementById('overlay');
    var margin = 0;
    
    overlay.style.position = "relative";
    var menu_elem = document.getElementsByClassName('menu')[0];
    if(!menu_elem || document.body.offsetHeight <= window.innerHeight + margin){
        if(more){
            more.style.visibility = "hidden"; }
        if(less){
            less.style.visibility = "hidden";
            less.classList.add('back');
            less.innerHTML = "&nbsp;";
            less.onclick = function(event){ menuclick(event,'back'); }}}
    else{
        if(less){
            if(window.scrollY <= margin){
                less.style.visibility = "hidden";
                less.classList.add('back');
                less.innerHTML = "&nbsp;";
                less.onclick = function(event){ menuclick(event,'back'); }}
            else{
                less.style.visibility = "inherit";
                less.classList.remove('back');
                less.innerHTML = "^";
                less.onclick = function(event){ seekLess(event); }}}
        if(more){
            if(window.scrollY + window.innerHeight + margin >= document.body.offsetHeight){
                more.style.visibility = "hidden"; }
            else{
                more.style.visibility = "inherit";
                more.classList.remove('back');
                more.innerHTML = "v";
                more.onclick = function(event){ seekMore(event); }}}}
    overlay.style.position = "absolute";
}

exports.loadpage = function(container, page_url, data){
    var req = new XMLHttpRequest();
    req.onreadystatechange = handle;
    req.open("GET", page_url);
    req.send();

    function handle(){
        if(req.readyState == 4){
            if(req.status == 200){
               var html = req.responseText;
               setClass(container, "opaque",true);
               setClass(container, "fade",false);
               setClass(container, "clear",false);
               container.innerHTML = html;
               refreshSeekButtons();
               window.onscroll = function(){ refreshSeekButtons(); }}
               //myScroll = new IScroll("#scrollwrap");
               //var scrollwrap = document.getElementById("scrollwrap");
               /*window.onhashchange = function(e){
                   scrollwrap.scrollTop = 0;
                   myScroll.scrollToElement(document.getElementsByName(location.hash.substr(1))[0]); 
                   location.hash = "";
                   e.preventDefault();
                   return false; }*/
            else{
               setClass(container, "opaque",false);
               setClass(container, "fade",true);
               setClass(container, "clear",false);
               exports.render(container, "alert",
                   { message: "Could not load " + page_url})}}
    }
}

exports.render = function(container, name,data){
    setClass(container, "opaque",false);
    setClass(container, "fade",true);
    setClass(container, "clear",false);
    if(menus.hasOwnProperty(name)){
        container.style.visibility = "visible";
        container.innerHTML = LispMarkup.toHtml(menus[name], data); }
    else{
        container.style.visibility = "hidden"; }
    refreshSeekButtons();
    window.onscroll = function(){ refreshSeekButtons(); }
}

})(exports);


