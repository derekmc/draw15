
var module = this;

(function(module){
    function T(){} // document types

    module.Display2D = Display2D;
    
    // ----------------------------------------------
    // Display2d
    //
    // Convert between a 2d geometric space and output display coordinates.
    // ----------------------------------------------

    function Display2D(init){
        if(!(this instanceof Display2D)){
             throw "Display2D must be called as constructor with 'new' keyword"; }

        var space_focus = [0,0]; // the focus of the geometric space
        var display_dim = [480, 320];
        var display_focus = [240, 160]; // the focus of the space is placed at the display focus
        var scale = 1; // each geometric unit is this many display units
        var scale_unit = "half-display-h"; // choose what type of units you want to use to scale display coordinates
        var scale_unit_regex = /pixel|display-w|display-h|half-display-w|half-display-h/;
        var scale_mult = 1;  // computed from scale, scale_unit, and output_dim

        // private funcs
        function recompute_scale(){
            T(scale_unit, scale_unit_regex);  // double check scale unit

            if(scale_unit == "pixel"){
                scale_mult = scale; }
            if(scale_unit == "display-w"){
                scale_mult = scale * display_dim[0]; }
            if(scale_unit == "display-h"){
                scale_mult = scale * display_dim[1]; }
            if(scale_unit == "half-display-w"){
                scale_mult = 0.5 * scale * display_dim[0]; }
            if(scale_unit == "half-display-h"){
                scale_mult = 0.5 * scale * display_dim[1]; }}
            

        // getter functions
        this.getDisplayDim = function(result){
            if(!result) result = [0,0];
            result[0] = display_dim[0];
            result[1] = display_dim[1];
            return result; }
        this.getScale = function(){
            return scale; }
        this.getScaleUnit = function(){
            return scale_unit; }
        this.getSpaceFocus = function(result){
            if(!result) result = [0,0];
            result[0] = space_focus[0];
            result[1] = space_focus[1];
            return result; }
        this.getDisplayFocus = function(result){
            if(!result) result = [0,0];
            result[0] = display_focus[0];
            result[1] = display_focus[1];
            return result; }
            
        
               

        // setter functions
        this.setSpaceFocus = function(_space_focus){
            T(_space_focus, [0,0]);
            space_focus[0] = _space_focus[0];
            space_focus[1] = _space_focus[1]; }

        this.setDisplayFocus = function(_display_focus){
            T(_display_focus, [0,0]);
            display_focus[0] = _display_focus[0];
            display_focus[1] = _display_focus[1]; }

        this.setDisplayWidth = function(_width){
            T(_width, 0);        
            display_dim[0] = _width;
            recompute_scale(); }

        this.setDisplayHeight = function(_height){
            T(_height, 0);
            display_dim[1] = _height;
            recompute_scale(); }

        this.setDisplayDim = function(_display_dim){
            T(_display_dim, [0,0]);
            display_dim[0] = _display_dim[0];
            display_dim[1] = _display_dim[1];
            recompute_scale(); }

        this.setScaleUnit = function(_scale_unit){
            T(_scale_unit, scale_unit_regex);
            scale_unit = _scale_unit;
            recompute_scale(); }

        this.setScale = function(_scale){
            T(_scale, 0);
            scale = _scale;
            recompute_scale(); }


        // modify object
        this.moveSpaceFocus = function(dx, dy){
            //T(delta, [0,0]);
            space_focus[0] += dx;
            space_focus[1] += dy; }


        // conversions and computations
        this.displayToSpace = function(result, v){
            //T(result, null, [0,0]); definitely need to comment out typechecks involving memory allocations for functions that will be frequently called.
            //T(v, [0,0]);
            if(!result) result = [0,0];
            result[0] = (v[0] - display_focus[0]) / scale_mult + space_focus[0];
            result[1] = (v[1] - display_focus[1]) / scale_mult + space_focus[1];
            return result;
        }
     
        this.spaceToDisplay = function(result, v){
            //T(result, null, [0,0]);
            //T(v, [0,0]);
            if(!result) result = [0,0];
            result[0] = (v[0] - space_focus[0]) * scale_mult + display_focus[0];
            result[1] = (v[1] - space_focus[1]) * scale_mult + display_focus[1];
            return result;
        }
        this.save = function(){
            return {
                space_focus: space_focus,
                display_focus: display_focus,
                display_dim: display_dim,
                scale_unit: scale_unit,
                scale: scale
            }
        }
        if(init){
            if(init.hasOwnProperty("space_focus")){
                this.setSpaceFocus(init.space_focus); }
            if(init.hasOwnProperty("display_focus")){
                this.setDisplayFocus(init.display_focus); }
            if(init.hasOwnProperty("display_dim")){
                this.setDisplayDim(init.display_dim); }
            if(init.hasOwnProperty("scale_unit")){
                this.setScaleUnit(init.scale_unit); }
            if(init.hasOwnProperty("scale")){
                this.setScale(init.scale); }
        }
        recompute_scale();
    }
})(module);
