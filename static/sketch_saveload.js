
var exports = window.SketchSaveLoad = {};

(function(exports){
var STORAGE_VERSION = "0";

exports.saveState = saveState;
exports.loadState = loadState;
exports.saveFile = saveFile;
exports.loadFile = loadFile;


var persistent_config_list = [
     "tool_index",
     "pen_list", "pen_index", "pen_width",
     "draw_line_enabled",
     "pencil_opacity",
     "pen_width",
     //"transparency",
     //"scene_visibility",
     "filename",
]



function saveFile(state, filename, error, next){
    var data = {}
    data.strokes = state.drawing.strokes;
    data.points = pointsToString(state.drawing.points, state.drawing.point_index);
    data.undo = state.undo;
    var config = data.config = [];
    for(var i=0; i < persistent_config_list.length; ++i){
        var property = persistent_config_list[i];
        var value = state[property];
        config.push(property);
        config.push(value);
    }
    data.storage_version = STORAGE_VERSION
    var blob = new Blob([JSON.stringify(data)],{type:"text/plain;charset=utf-8 "});
    saveAs(blob,filename);
    if(next) next();
}

function loadFile(state, file, error, next){
    var fr = new FileReader();
    fr.onload = onLoad;
    fr.readAsText(file);
    function onLoad(){
        var data = JSON.parse(fr.result);
        if(!data.hasOwnProperty("storage_version") || data.storage_version == "0"){
            var points = stringToPoints(data.points);
            if(data.undo) state.undo = data.undo;
            state.drawing.strokes = data.strokes;
            state.drawing.points = points;
            state.drawing.points_size = points.length;
            state.drawing.point_index = points.length/2;
            state.zoomindex = 2;
            var config = data.config;
            for(var i=0; i<config.length; i+=2){
                var property = config[i];
                var value = config[i+1];
                if(persistent_config_list.indexOf(property) > 0){
                    if(value !== null && value !== undefined){
                        state[property] = value; }}}
            if(next) next();
        }
        else{
            if(error) error("Unknown \"storage_version\" value : '" + data.storage_version + "'");
        }
    }
}

function pointsToString(points, count){
    var s = "";
    for(var i=0; i<2*count; ++i){
        s += points[i].toString(32) + ","; }
    s = s.substr(0, s.length-1);
    return s;
}

function stringToPoints(s){
    var s_arr = s.split(",");
    var arr = new Int32Array(s_arr.length);
    for(var i=0; i<s_arr.length; ++i){
        arr[i] = parseInt(s_arr[i], 32); }
    return arr;
}
// state includes everything but the contents of the documents,
// which must be loaded from storage.
function saveState(store, state, error, next){
    var assignments = {};
    //assignments[STORAGE_PREFIX + "lines"] = state.lines;
    assignments[STORAGE_PREFIX + "strokes"] = state.drawing.strokes;
    assignments[STORAGE_PREFIX + "undo"] = state.undo;
    assignments[STORAGE_PREFIX + "points"] =
        pointsToString(state.drawing.points, state.drawing.point_index);
    var config = [];
    for(var i=0; i < persistent_config_list.length; ++i){
        var property = persistent_config_list[i];
        var value = state[property];
        config.push(property);
        config.push(value);
    }
    assignments[STORAGE_PREFIX + "storage_version"] = STORAGE_VERSION;
    assignments[STORAGE_PREFIX + "config"] = config;
    store.set(assignments, next);
}
function loadState(state, store, error, next){
    var version_key = STORAGE_PREFIX + "storage_version";
    try{
        store.get([version_key], function(items){
            var storage_version = items[version_key];
            if(storage_version === null || storage_version === undefined || storage_version == "0"){
                var item_list = ["strokes", "points", "config","undo"];
                var key_list = item_list.map(function(item){
                    return STORAGE_PREFIX + item; });
                store.get(key_list, function(items){
                    if(items[key_list[0]] && items[key_list[1]]){ // strokes and points
                        var strokes = items[key_list[0]];
                        var points = stringToPoints(items[key_list[1]]);
                        state.drawing.strokes = strokes;
                        state.drawing.points = points;
                        state.drawing.points_size = points.length;
                        state.drawing.point_index = points.length/2;
                    }
                    if(items[key_list[2]]){ // config
                        var config = items[key_list[2]];
                        for(var i=0; i<config.length; i+=2){
                            var property = config[i];
                            var value = config[i+1];
                            if(persistent_config_list.indexOf(property) > 0){
                                if(value !== null && value !== undefined){
                                    state[property] = value; }}}
                    }
                    if(items[key_list[3]]){ // undo
                        state.undo = items[key_list[3]];
                    }
                    state.refresh = true;
                    next();
                })
            }
            else{
                throw new error("Unknown \"storage_version\" value : '" + data.storage_version + "'");
            }
        })
        state.refresh = true;
    }
    catch(e){
        error(e);
    }
}
})(exports);
