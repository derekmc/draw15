var exports = window.SketchInit = {};


(function(exports){
//var STORAGE_PREFIX = "draw15_";

exports.init_state = init_state;
exports.init_ui = init_ui;

function init_ui(){
    var ui = {
        canvas: document.getElementById("canvas"),
        foreground: document.createElement("canvas"),
        background: document.createElement("canvas"),
        buffer: document.createElement("canvas"),
        overlay: document.getElementById("overlay"),
        prevpage: document.getElementById("prevpage"),
        nextpage: document.getElementById("nextpage"),
        menulink: document.getElementById("menulink"),
        undo: document.getElementById("undo"),
        redo: document.getElementById("redo"),
        zoomin: document.getElementById("zoomin"),
        zoomout: document.getElementById("zoomout"),
        joystick: document.getElementById("joystick"),
        colorwheel: document.getElementById("colorwheel"),
        toolcycle: document.getElementById("toolcycle"),
        erasebutton: document.getElementById("erasebutton"),
        linebutton: document.getElementById("linebutton"),
        //zoomtoggle: document.getElementById("zoomtoggle"),
        bottom: document.getElementById("bottom"),
        tools: document.getElementById("tools"),
        nav: document.getElementById("nav"),
	    banner: document.getElementById("banner"),
        banner_text: document.getElementById("banner_text"),
        banner_button: document.getElementById("banner_button"),
        status: document.getElementById("status"),
    }
    return ui;
}



function init_state(state, ui){
    var width = ui.canvas.width;
    var height = ui.canvas.height;

    if(!state) state = {}
    var state_default = {
        // drawing stuff
        drawing: new SketchDrawing.SketchDrawing(),
        lines: [], //[[pencolor, penwidth, x0, y0, x1, y1, ... ], ... ]
        backgroundColor: "#fff",

        // data stuff
        filename: "draw15.json",

        // ui stuff
        scene_fade_start: 0,
        scene_fade_period: 2500,
        scene_visibility: 100,
        scene_visibility_max: 100,
        scene_display_focus_ratio: [1/4, 1/4],
        animateinterval: null,
        saveinterval: null,
        animate_delay: 38,
        animate_redraw_counter: 0,
        animate_redraw_frame_count: 5,
        hide_ui: false,
        status: "",
        status_expire: 0,
        refresh: true,  // triggers a full refresh on the next animate call.
        zoom_redraw_focus: [0,0],
        scene_redraw_focus: [0,0],
        mousedown: null,
        mousedowntime: 0,
        menu_stack: [],

        // tool stuff
        erase: false,
        draw_line_enabled: false,
        pencil_enabled: false,
        //tool_list : ['Pencil', 'Pen'],
        //tool_index : 0,
        remove_width: 15,
        //remove_radius: 8,
        drawfore: true,
        tool_width_max : 50,
        // pen widths are absolute at zoom x1
        pen_index: 0,
        pen_width: 3,
        pencil_opacity: 0.3,
        pencil_opacity_max: 10,
        pen_brightness_max: 100,
        pen_list: [ // {color, width, brightness, (transparency)}
           {color:[0,0,0], brightness:0, opacity: 0.3}, // black
           {color:[255,0,0], brightness:0.5, opacity: 0.3},  // red
           {color:[255,102,0], brightness:0.5, opacity: 0.3},  // orange
           {color:[255,255,0], brightness:0.5, opacity: 0.3},  // yellow
           {color:[0,255,0], brightness:0.5, opacity: 0.3},  // green
           {color:[0,0,255], brightness:0.5, opacity: 0.3},  // blue
           {color:[153,0,255], brightness:0.5, opacity: 0.3},  // purple
           //{color:[255,0,128], brightness:0.5},  // violet
        ],
        undo: {
            history: [],// [action, [target, datastructure], . . .] actions: newline, removeline, cutline
            index: 0, },
        joystick: {
            speed: 2.5, // sensitivity, ie. units of pixel change per second for each pixel of joystick displacement
            dragging: false,
            offset: null},  // position relative to mousedown
        colorwheel: {
            dragmode: DRAG_NONE, // 0 - not dragging, 1 - horizontal, 2 - vertical
            modifier_incr: 0.003,  // vertical
            width_incr: 0.15,  // horizontal
            last_modifier_value: null,
            lastwidth: null,
            offset: null},

        // display stuff
        zoom_display: new Display2D({
            display_dim: [width, height],
            scale_unit: "pixel",
            scale: 1,
            space_focus: [0,0],
            display_focus: [Math.floor(width/2), Math.floor(height/2)]}),
        scene_display: new Display2D({
            display_dim: [width, height],
            scale_unit: "pixel",
            scale: 1,
            space_focus: [0,0],
            display_focus: [Math.floor(width/2), Math.floor(height/2)]}),
        width: width,
        height: height,
        ZOOM_MAX: 64,
        ZOOM_MIN: 1/16,
        WINDOW_ZOOM_MAX_RATIO: 32,
        ZOOM_FACTOR: 2,
        ZOOM_GRAB_WIDTH: 18, 
        zoomspeedfactor: 1.2,
        show_scene: false,
        scene_ratio: 4,
        scene_ratio_index: 0,
        scene_ratios: [4, 8],
        scene_window_drag_start: null,
    }
    for(var k in state_default){
        state[k] = state_default[k]; }
    return state;
}

})(exports);
