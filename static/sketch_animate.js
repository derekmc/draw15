
var exports = window.SketchAnimate = {};

(function(exports){

exports.animate = animate;

//during certain states the UI must be animated, ie. both the state and interface refreshed repeatedly on a certain clock interval.
//whenever the application does not require graphical updates, this function returns without doing anything.
function animate(state,ui ){
    var suppress_redraw = false;
    if(state.joystick.offset &&
       (state.joystick.offset[0] || state.joystick.offset[1])){
        state.refresh = true;
        ++state.animate_redraw_counter;
        if(state.animate_redraw_counter == state.animate_redraw_frame_count){
            state.animate_redraw_counter = 0; }
        if(state.animate_redraw_counter != 0){
            suppress_redraw = true; }
        var zoom_speed = Math.pow(state.zoomspeedfactor, Math.log(state.zoom_display.getScale()) / Math.log(state.ZOOM_FACTOR));
        var s = zoom_speed * state.joystick.speed * state.animate_delay * 0.001,
            dx = s * state.joystick.offset[0],
            dy = s * state.joystick.offset[1];
        var zoom_scale = state.zoom_display.getScale();
        dx /= zoom_scale;
        dy /= zoom_scale;

        state.zoom_display.moveSpaceFocus(dx,dy);
        state.scene_display.moveSpaceFocus(dx,dy);
        //state.zoom_center[0] += dx;
        //state.zoom_center[1] += dy;
        // checkbounds
        SketchGeometry.fitZoomBox(state);
        state.status = "";
        window.requestAnimationFrame(function(){ SketchDisplay.refreshUI(ui, state); });
        //state.status_expire = Date.now() + STATUS_TIMEOUT;
        //state.status = "(" + state.zoom_display.getSpaceFocus().map(function(x){ return Math.floor(x); }).join(", ") + ")";
    }
    /*if(state.show_scene && !state.joystick.offset && !state.mousedown){
        // fade the zoom overlay in and out.
        var cycle_length = state.scene_fade_period;
        var now = Date.now();
        if(now < state.scene_fade_start + cycle_length){ // one cycle only
            window.requestAnimationFrame(function(){
                var phase = ((now - state.scene_fade_start) % cycle_length) / cycle_length;
                SketchDisplay.sceneFadeAnimate(ui, state,phase); })}
        else if(state.scene_fade_start != 0){
            state.scene_fade_start = 0;
            window.requestAnimationFrame(function(){
                SketchDisplay.refreshAll(ui,state); }); }
    }*/
    if(state.refresh){
        state.refresh = false;
        if(state.status && state.status.length && state.status_expire < Date.now()){
            state.status = "" }
        SketchGeometry.fitZoomBox(state);
        window.requestAnimationFrame(function(){ SketchDisplay.refreshAll(ui, state, suppress_redraw); });
    }
    else if(state.status && state.status.length && state.status_expire < Date.now()){
        state.status = "";
        window.requestAnimationFrame(function(){ SketchDisplay.refreshUI(ui, state); });
    }
}
})(exports);
