
this.addEventListener('fetch', fetch_handle);
//this.addEventListener('install', install_handle);
//function install_handle(){}
const SW_VERSION = "Draw15 Service Worker 0.0";
const SERVER_TIMEOUT = 1200;
const DEBUG = false;


function addCommentToResponse(response){
    let prefix = "";
    let params = {
        status: response.status,
        statusText: response.statusText,
        headers: {}
    }
    response.headers.forEach(function(v,k){
        params.headers[k] = v; })

    if(response.url.match(/\.(js|css)$/)){
        prefix = "/* Served with service worker: " + SW_VERSION + " */"; }
    else if(response.url.match(/\.html$/)){
        prefix = "<!-- Served with service worker: " + SW_VERSION + " -->"; }

    return response.text().then(function(body){
        return new Response(prefix + body, params);
    })
}

function fetch_handle(event){
    var request = event.request;
    // default response
    if(request.url.match(/\/available_version\.js$/)){
        // for available_version.js, if r
        var response_winner = null;
        var timeout_promise = new Promise(function(resolve){
            setTimeout(function(){
                    if(!response_winner){
                        response_winner = "timeout";
                        console.log("'available_version.js': response winner '" + response_winner + "'.");
                    }
                    resolve(new Response("")); 
                }, SERVER_TIMEOUT);
        })

        var fetch_promise = fetch(event.request).then(function(response){
            if(!response_winner){
                response_winner = "fetch";
                console.log("'available_version.js': response winner '" + response_winner + "'.");
            }
            //caches.open('mycache').then(function(cache){
                //cache.put(event.request, response.clone());
            //})
            return response.clone();
        })

        event.respondWith(Promise.race(
            [timeout_promise, fetch_promise]))
    }
    else{
        event.respondWith(
            caches.match(request.url).then(function(response){
                if(response){
                    DEBUG && console.log("Serving " + request.url + " from cache using service_worker.js");
                    return addCommentToResponse(response);
                }
                return fetch(event.request);
            })
        )
    }
}

