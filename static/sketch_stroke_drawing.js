//var exports = window.SketchStrokeDrawing = {};


(function(exports){

exports.SketchStrokeDrawing = SketchStrokeDrawing;



// a stroked
function SketchStrokeDrawing(){
    if(!(this instanceof SketchStrokeDrawing)){
        throw "SketchStrokeDrawing must be called as constructor with 'new' keyword"; }
    var MAX_STROKE_DELTA = (1 << 15) - 1;
    console.log(MAX_STROKE_DELTA);

    //this.point_grain = 0x40;  // 1/(size of pixel)
    // all properties / info / data / state for this object.
    // capitalize to distinguish that this variable has object level scope
    var State = this.State = {
        top_layer_has_erase_stroke: false,
        default_grain: 0x40, // used for stroke starting points
        last_stroke_data: null,
        last_stroke_info: null,
        last_stroke_background: false,
        last_x: 0,
        last_y: 0,
        stroke_index: 1, // stroke index of 0 is reserved for zero length stroke
        layers: [[]],
        stroke_map: {}, // map of stroke data arrays. {index: int32array}
    }



    //this.layers = [[]]; // [layer]; layer: [stroke_info]; stroke_info: {stroke_index, x0, y0, style, width}] 
    this.clear = function(){
        State.stroke_index = 0;
        State.last_stroke_data = null;
        State.layers.length = 0;
        State.stroke_map = {};
    }
    // negative grains are fractions.
    this.numToGrain = function(x, grain){
        if(grain < 0) grain = 1/-grain;
        return Math.floor(x * grain);
    }
    this.grainToNum = function(n, grain){
        if(grain < 0) grain = 1/-grain;
        return (0.0 + n)/grain;
    }
    // params: {style, width, grain, background?, erase?}
    this.startStroke = function(_x, _y, params){
        var grain = params.grain? params.grain : State.default_grain;
        var x = this.numToGrain(_x, grain);
        var y = this.numToGrain(_y, grain);
        var stroke_info = {
            stroke_index: null, // null index for last stroke.
            x0: x,
            y0: y,
            grain: grain,
            scale: params.scale? params.scale : 1,
            style: params.style? params.style : "",
            width: params.width? params.width : 1,
        }
        var last_stroke = State.last_stroke_data;
        // reset last stroke data
        State.last_stroke_data = [];
        if(last_stroke != null && last_stroke.length && State.last_stroke_info){
            console.log('setting stroke index');
            State.last_stroke_info.stroke_index = State.stroke_index; }
        State.last_stroke_info = stroke_info;
        var background = params.background;
        var erase = (params.style == ""); // empty string indicates 'erase' stroke.
        var layers = State.layers;

        if(background){
            if(erase){
                throw "SketchStrokedrawing.startStroke: cannot erase on background"; }}
        if(last_stroke != null && last_stroke.length){
            State.stroke_map[State.stroke_index] = new Int16Array(last_stroke);
            ++State.stroke_index; }
        if(background){
            if(State.top_layer_has_erase_stroke){
                State.top_layer_has_erase_stroke = false;
                layers.unshift([]); }
            layers[0].unshift(stroke_info);
        }
        else{
            // if we erase with an even number of layers,
            //  we need a new top layer,
            //  so the erase stroke covers all existing layers.
            if(erase) State.top_layer_has_erase_stroke = true;
            if(erase && layers.length % 2 == 0){
                layers.push([]); }
            layers[layers.length - 1].push(stroke_info);
        }
        
        State.last_x = x;
        State.last_y = y;
    }
    this.addPoint = function(_x,_y){
        var data = State.last_stroke_data;
        if(data == null){
            return false; }

        var x = this.numToGrain(_x, State.last_stroke_info.grain);
        var y = this.numToGrain(_y, State.last_stroke_info.grain);

        console.log('last x, y:', State.last_x, State.last_y);
        var dx = x - State.last_x;
        var dy = y - State.last_y;
        console.log('_x _y dx dy grain:', _x, _y, dx, dy, State.last_stroke_info.grain);
        while(true){ // break up line if too long.
            var max = MAX_STROKE_DELTA;
            if((dx > 0? dx > max : dx < -max) ||
               (dy > 0? dy > max : dy < -max)){
                _dx = dx > 0? Math.min(dx, max) : Math.max(dx, -max);
                _dy = dy > 0? Math.min(dy, max) : Math.max(dy, -max);
                dx -= _dx;
                dy -= _dy;
                console.log('_dx _dy', _dx, _dy);
                data.push(_dx, _dy); }
           else{
               console.log('dx dy', dx, dy);
               data.push(dx, dy);
               break; }}

        State.last_x = x;
        State.last_y = y;
    }
    this.changeLastPoint = function(_x,_y){  //used by lineTool
        var data = State.last_stroke_data;
        if(data == null){
            return false; }

        var i = data.length - 2;
        if(i < 0) return false;

        var x = this.numToGrain(_x);
        var y = this.numToGrain(_y);

        var dx = x - State.last_x;
        var dy = y - State.last_y;

        if(dx == 0 && dy == 0) return false;

        // add this dx, dy, to stroke dx, dy.
        data[i] += dx;
        data[i+1] += dy;

        State.last_x = x;
        State.last_y = y;
        return true;
    }
    this.getLastPoint = function(){
        var last_stroke = State.last_stroke_data;
        if(!last_stroke) return null;
        var x = this.grainToNum(State.last_x);
        var y = this.grainToNum(State.last_y);
        return [ x, y ];
    }
    // iterate over strokes, from the middle layers out
    // (foreground and background layers are paired for erase to work properly)
    // after each foreground/background layer pair, layer_callback is called.
    this.stroke_iter = function(iter, stroke_callback, layer_pair_complete_callback){
        var layers = State.layers;
        if(!iter.hasOwnProperty("layer_number")){
            iter.layer_number = 0;
            iter.stroke_layer_index = 0;
            iter.background = layers.length > 1; }

        var layer_index = get_layer_index(iter, layers);
        var layer = layers[layer_index];
        //console.log('layer_index', layer_index, 'iter', iter, layers.length);
        if(!layer){
            //stroke_callback(this.last_stroke_info, this.last_stroke_data);
            return false;
        }
        //console.log('here', iter.stroke_layer_index, layer);
        var go_next_layer = iter.stroke_layer_index >= layer.length;
        if(go_next_layer){
            iter.stroke_layer_index = 0;
            if(iter.background){
                iter.background = false; }
            else{
                layer_pair_complete_callback(iter.layer_number);
                ++iter.layer_number;
                iter.background = true; }
            layer_index = get_layer_index(iter, layers);
            layer = layers[layer_index];
        }
        if(layer){
            var stroke_info = layer[iter.stroke_layer_index];
            var stroke_index = stroke_info.stroke_index;
            var stroke_data;
            // callback should handle normal array or Int16Array
            if(stroke_index == null){
                // empty layer should only happen on last layer.
                if(State.last_stroke_data == null){
                    if(layer_index != layers.length%2 == 0? layers.length - 1 : 0){
                        throw new Error("empty layer was not last layer"); }j
                    return false; }
                else{
                    stroke_data = State.last_stroke_data; }}
            else{
                stroke_data = State.stroke_map[stroke_index]; }
            stroke_callback(stroke_info, stroke_data);
            ++iter.stroke_layer_index;
        }
        
        return true;
        /*
        if(layer_index == layers.length-1 && iter.stroke_layer_index >= layer.length){
            return false; }
        else{
            return true; }
        */
        function get_layer_index(iter, layers){
            return Math.floor(layers.length/2) +
                   (iter.background? -iter.layer_number-1: iter.layer_number);
        }
    }
}

test();

function test(){
    var drawing = new SketchStrokeDrawing();
    console.log(drawing);
    // params: {style, width, grain, background?, erase?}
    drawing.startStroke(0,0, {style:"rgba(0,0,255,0.25)",
                              width:3,
                              grain:64});
    drawing.addPoint(10,10);
    drawing.startStroke(0,0, {style:"rgba(0,0,255,0.25)",
                              width:5,
                              grain:64});
    drawing.addPoint(0.5,0.345);
    //console.log(drawing);
    console.log(JSON.stringify(drawing));
    var iter = {};
    while(drawing.stroke_iter(iter, stroke_callback, layer_callback));
    //console.log(JSON.stringify(drawing.layers));

    console.log("done");
    function stroke_callback(stroke_info, stroke_data){
        console.log("stroke", JSON.stringify(stroke_info), stroke_data);
    }
    function layer_callback(){
        console.log("layer");
    }
    /*
    */
}


})(exports);
