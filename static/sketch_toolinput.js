
var exports = window.SketchToolInput = {};

(function(exports){
exports.removeTool = removeTool;
exports.joystickDown = joystickDown;
exports.joystickLongpress = joystickLongpress;
exports.colorWheelDown = colorWheelDown;
exports.mouseDownCanvas = mouseDownCanvas;
exports.colorWheelLongpress = colorWheelLongpress;
exports.mouseMove = mouseMove;

exports.mouseUp = mouseUp;

//SketchDisplay
var refreshAll = SketchDisplay.refreshAll;
var refreshUI = SketchDisplay.refreshUI;
var drawLastStroke = SketchDisplay.drawLastStroke;
var drawCursor = SketchDisplay.drawCursor;

//SketchGeometry
var canvasToScreen = SketchGeometry.canvasToScreen;
var canvasToScreenZoomed = SketchGeometry.canvasToScreenZoomed;
var screenZoomedToCanvas = SketchGeometry.screenZoomedToCanvas;
var screenToCanvas = SketchGeometry.screenToCanvas;
var getStateColor = SketchColor.getStateColor;
var getStateColorOpaque = SketchColor.getStateColorOpaque;
var fitZoomBox = SketchGeometry.fitZoomBox;
var applyToolWidthLimits = SketchGeometry.applyToolWidthLimits;

function computeCuts(drawing, cut){
}

// tool functions
function removeTool(ui,state, x,y){
    var width_changed = applyToolWidthLimits(state);
    var pos = screenZoomedToCanvas(null, state, [x,y]);
    var x = pos[0];
    var y = pos[1];
    var remove_dist = state.remove_width/2;;
    var remove_index = -1;

    var strokes = state.drawing.strokes;
    var points = state.drawing.points; 
    var grain = state.drawing.point_grain;

    // vectors used to find dist between point and line segment.
    // initialize outside loop and reuse datastructure to save mem allocs.
    var a = [0,0];
    var b = [0,0];
    var u = [0,0];
    var v = [0,0];
    var offset = [0,0];
    var p = [0,0];

    for(var i=0; i<strokes.length; ++i){
        var remove = false;
        var linewidth = strokes[i][1];
        var start = strokes[i][2];
        var end = strokes[i][3];

        if(start == end){
            a[0] = points[2*start]/grain;
            a[1] = points[2*start+1]/grain;
            d = Vector.dist(pos,a);
            d -= linewidth/2;
            if(d < 0) d = 0;
            if(d <= remove_dist){
                remove_index = i;
                remove_dist = d; }}
        for(var j=start; j<end; ++j){
            // find distance from pos to line segment ab
            a[0] = points[2*j]/grain;
            a[1] = points[2*j+1]/grain;
            b[0] = points[2*j+2]/grain;
            b[1] = points[2*j+3]/grain;
            Vector.minus(u, pos,a);
            Vector.minus(v, b,a);
            var dv = Vector.norm(v);
            Vector.scale(offset, Vector.dot(u,v)/dv/dv,v);
            Vector.add(p, offset,a);

            if(Vector.dot(u,v) < 0){ // a is closest to pos
                Vector.assign(p, a); }
            else if(Vector.norm(v) < Vector.norm(offset)){ // b is closest to pos
                Vector.assign(p, b); }
            d = Vector.dist(pos,p);
            d -= linewidth/2;    
            if(d < 0) d = 0;            
            if(d <= remove_dist){
                remove_index = i;
                remove_dist = d;}}}
    if(remove_index >= 0){
        var removed = strokes.splice(remove_index, 1);
        state.undo.history[state.undo.index] = ["removeline", remove_index, removed[0]];
        ++state.undo.index;
        state.undo.history.length = state.undo.index;
        window.requestAnimationFrame(function(){
            if(width_changed){
                var zoomlevel = state.show_scene? state.scene_display.getScale() : state.zoom_display.getScale();
                state.status = "Remove Width: " + Math.round(zoomlevel * state.remove_width) + (zoomlevel > 1? "/" + zoomlevel : "");
                state.status_expire = Date.now() + STATUS_TIMEOUT; }
            refreshAll(ui, state); }); }
    else if(width_changed){
        var zoomlevel = state.show_scene? state.scene_display.getScale() : state.zoom_display.getScale();
        state.status = "Remove Width: " + Math.round(zoomlevel * state.remove_width) + (zoomlevel > 1? "/" + zoomlevel : "");
        state.status_expire = Date.now() + STATUS_TIMEOUT;
        refreshUI(ui, state); }
}
function joystickDown(ui,state, x,y){
    state.mousedowntime = Date.now();
    state.mousedown = [x,y];
    state.joystick.offset = [0,0];
    state.joystick.dragging = false;
    window.requestAnimationFrame(function(){ refreshAll(ui, state); });
}
function joystickTap(ui,state, x,y){
    var zoom_value = state.zoom_display.getScale();
    if(zoom_value < 1) zoom_value = 1;
    var offset = state.width/zoom_value/2;
    state.zoom_display.moveSpaceFocus(offset, 0);
    state.scene_display.moveSpaceFocus(offset, 0);
    fitZoomBox(state);
    state.status = "";
    if(state.show_scene){
        state.scene_fade_start = Date.now(); }
    refreshAll(ui, state);
}
function joystickLongpress(ui,state, x,y){
/*
    if(!state.joystick.dragging && !state.show_scene){
        state.mousedown = null;
        state.joystick.offset = null;
        state.joystick.dragmode = DRAG_NONE;
        state.show_scene = true;
        var scene_scale = state.scene_display.getScale();
        var scale = state.zoom_display.getScale();
        state.status = "Scene Draw";
        state.scene_fade_start = Date.now();
        var new_scale = scale * state.scene_ratios[state.scene_ratio_index];
        state.scene_display.setScale(scale);
        state.zoom_display.setScale(new_scale);
        state.scene_display.setSpaceFocus(state.zoom_display.getSpaceFocus());
        fitZoomBox(state, true);
        refreshAll(ui, state);
    }
*/
}
function colorWheelDown(ui,state, x,y){
    state.mousedowntime = Date.now();
    state.mousedown = [x,y];
    state.colorwheel.offset = [0,0];
}

function colorWheelLongpress(ui,state){
    if(state.colorwheel.offset &&
       state.colorwheel.dragmode == DRAG_NONE &&
       !state.erase){
        state.mousedown = null;
        state.colorwheel.offset = null;
        state.colorwheel.dragmode = DRAG_NONE;
        // duplicated in SketchMenuActions
        state.drawfore = !state.drawfore;
        state.status = (state.drawfore? "Foreground " : "Background");
        state.status_expire = Date.now() + STATUS_TIMEOUT;
        refreshUI(ui, state);
    }
}
function mouseDownCanvas(ui,state, x,y){
    // stop scene fade if running.
    state.scene_fade_start = 0;
    if(state.hide_ui){
        state.hide_ui = false;
        window.requestAnimationFrame(function(){ refreshAll(ui, state); });
        return; }
    state.mousedowntime = Date.now();
    state.mousedown = [x,y];
    state.last_mouse_pos = [x,y];
    if(state.erase){
        removeTool(ui,state, x,y);
        return; }

        
    if(state.show_scene){
        var corner = SketchGeometry.zoomWindowBounds(state);
        corner.length = 2;
        var grab_radius = state.ZOOM_GRAB_WIDTH/2;
        var dx = Math.abs(x - corner[0]);
        var dy = Math.abs(y - corner[1]);
        if(dx < grab_radius && dy < grab_radius){
            state.scene_window_drag_start = SketchGeometry.zoomWindowBounds(state);
            return; }
        else{
            setTimeout(function(){ document.body.classList.add("no_cursor"); }, 0); }
    }
    var width_changed = applyToolWidthLimits(state);
    var pos = screenZoomedToCanvas(null, state, [x,y]); // no need to reuse datastructure
    var pen = state.pen_list[state.pen_index];
    var d = state.drawing;
    d.addPoint(pos[0], pos[1]);
    var newstroke = [getStateColor(state), state.pen_width, d.point_index-1, d.point_index-1];
    var drawfore = state.drawfore;
    if(drawfore){
        d.strokes.push(newstroke);
        var index = d.strokes.length - 1; }
    else{
        var index = 0;
        d.strokes.unshift(newstroke); }
    state.undo.history[state.undo.index] = ["newline", index, newstroke];
    ++state.undo.index;
    //var ctx = ui.buffer.getContext("2d");
    //ctx.clearRect(0,0,canvas.width,canvas.height);
    //ctx.drawImage(ui.canvas,0,0);
    state.undo.history.length = state.undo.index;
    if(state.draw_line_enabled){
         d.addPoint(pos[0]+1, pos[1]);
         ++newstroke[3]; }
    window.requestAnimationFrame(function(){
        if(width_changed){
            var zoomlevel = state.show_scene? state.scene_display.getScale() : state.zoom_display.getScale();
            state.status = "Width: " + Math.round(zoomlevel * state.pen_width) + (zoomlevel > 1? "/" + zoomlevel : "");
            state.status_expire = Date.now() + STATUS_TIMEOUT;
            refreshUI(ui, state); }
        drawLastStroke(ui,state); })
}
function mouseMove(ui,state, x,y){
    if(state.mousedown){
        state.last_mouse_pos = [x,y];
        if(state.scene_window_drag_start){
            var w = ui.canvas.width;
            var h = ui.canvas.height;
            var scene_window = state.scene_window_drag_start.slice(0);
            var down = state.mousedown;
            var dx = x - down[0];
            var dy = y - down[1];
            // enforce bounds.
            var x0 = scene_window[0] + dx;
            var y0 = scene_window[1] + dy;
            var x1 = scene_window[2] + dx;
            var y1 = scene_window[3] + dy;
            if(x0 < 0) dx -= x0;
            if(x1 > w) dx -= (x1 - w);
            if(y0 < 0) dy -= y0;
            if(y1 > h) dy -= (y1 - h);
            scene_window[0] += dx;
            scene_window[1] += dy;
            scene_window[2] += dx;
            scene_window[3] += dy;
            var zoomed = state.zoom_display;
            var scene = state.scene_display;
            var display_focus = SketchGeometry.sceneWindowDisplayFocus(ui, scene_window);
            var space_focus = zoomed.displayToSpace(null, display_focus);
            scene.setDisplayFocus(display_focus);
            //zoomed.setSpaceFocus(space_focus);
            //scene.setSpaceFocus(space_focus);
            state.scene_display_focus_ratio[0] = display_focus[0]/w;
            state.scene_display_focus_ratio[1] = display_focus[1]/h;

            refreshAll(ui, state);
            /*
            state.zoom_display.setDisplayFocus(
            var bounds = SketchGeometry.zoomWindowBounds(state);
            var boxw = bounds[2] - bounds[0];
            var boxh = bounds[3] - bounds[1];

            var fx = scene_window[0] + dx;// + dx/w;
            var fy = scene_window[1] + dy;// + dy/h;
            var disp_focus = [fx, fy];
            
            //scene.setSpaceFocus(zoomed.displayToSpace(null, disp_focus));
            //zoomed.setSpaceFocus(zoomed.displayToSpace(null, disp_focus));
            scene.setDisplayFocus(disp_focus);
            // new focus must be recomputed before moving display focus.
            zoomed.setDisplayFocus(disp_focus);
            //zoomed.setSpaceFocus(zoomed.displayToSpace(null, disp_focus));
            */
        }
        else if(state.joystick.offset){
            var down = state.mousedown,
                dx = x - down[0],
                dy = y - down[1];
            if(Math.abs(dx) >= MINDRAG || Math.abs(dy) >= MINDRAG){
                state.joystick.dragging = true;}
            if(state.joystick.dragging){
                state.joystick.offset[0] = dx;
                state.joystick.offset[1] = dy;}
        }
        else if(state.colorwheel.offset){
            colorWheelMove(ui,state, x,y);
        }
        else{
            if(state.erase){
                removeTool(ui,state, x,y); }
            else if(state.draw_line_enabled){
                lineTool(ui,state, x,y); }
            else{
                penTool(ui,state, x,y); }
        }
    }
}

function lineTool(ui,state, x,y){
    var zoom = state.zoom_display.getScale();
    var pos = screenZoomedToCanvas(null, state,[x,y]);
    var d = state.drawing;
    if(d.changeLastPoint(pos[0], pos[1])){
        window.requestAnimationFrame(function(){
            drawLastStroke(ui, state); })}
}
function penTool(ui,state, x,y){
    applyToolWidthLimits(state);
    var pos = screenZoomedToCanvas(null, state,[x,y]); // no need to reuse datastructure.
    // new drawing datastructure stuff
    var d = state.drawing;
    var lastpoint = d.getPoint(-1);
    var drawfore = state.drawfore;
    var laststroke = d.strokes[drawfore? d.strokes.length-1 : 0];
    if(!lastpoint || !Vector.equal(pos, lastpoint)){
        d.addPoint(pos[0], pos[1]);
        ++laststroke[3]; //extend laststroke to include last point                 
        window.requestAnimationFrame(function(){
            drawLastStroke(ui,state); })}
}
function colorWheelMove(ui,state, x,y){
    var dx = x - state.mousedown[0],
        dy = y - state.mousedown[1];
    state.colorwheel.offset[0] = dx;
    state.colorwheel.offset[1] = dy;
    var pen = state.pen_list[state.pen_index];
    var zoomlevel = state.zoom_display.getScale();
    if(zoomlevel < 1) zoomlevel = 1;
    if(state.colorwheel.dragmode == DRAG_NONE){
        if(Math.abs(dx) > MINDRAG){
            state.mousedown[0] = x;
            state.mousedown[1] = y;
            state.colorwheel.offset[0] = 0;
            state.colorwheel.offset[1] = 0;
            state.colorwheel.dragmode = DRAG_HORIZONTAL;
            state.colorwheel.lastwidth = Math.round(state.erase? zoomlevel * state.remove_width : zoomlevel * state.pen_width); }
        else if(Math.abs(dy) > MINDRAG){
            state.mousedown[0] = x; // reset center
            state.mousedown[1] = y;
            state.colorwheel.offset[0] = 0;
            state.colorwheel.offset[1] = 0;
            state.colorwheel.dragmode = DRAG_VERTICAL;
            state.colorwheel.last_modifier_value = state.pencil_enabled? state.pencil_opacity : pen.brightness; }}
    if(state.colorwheel.dragmode == DRAG_HORIZONTAL){
        var dwidth = state.colorwheel.width_incr * state.colorwheel.offset[0];
        if(state.erase){
            state.remove_width = Math.round(dwidth + state.colorwheel.lastwidth)/zoomlevel;
            if(state.remove_width < 1/zoomlevel){
                // reset the lastwidth at mousedown point because we dragged past normal bounds.
                state.colorwheel.lastwidth = 1 - dx * state.colorwheel.width_incr;
                state.remove_width = 1/zoomlevel; }
            if(state.remove_width > state.tool_width_max/zoomlevel){
                state.colorwheel.lastwidth = state.tool_width_max - dx * state.colorwheel.width_incr;
                state.remove_width = state.tool_width_max/zoomlevel; } 
            state.status = "Remove Width: " + Math.round(zoomlevel * state.remove_width) + (zoomlevel > 1? "/" + zoomlevel : "");
            state.status_expire = Date.now() + STATUS_TIMEOUT;
            window.requestAnimationFrame(function(){ refreshUI(ui, state); })}
        else{
            state.pen_width = Math.round(dwidth + state.colorwheel.lastwidth)/zoomlevel;
            if(state.pen_width < 1/zoomlevel){
                // reset the lastwidth at mousedown point because we dragged past normal bounds.
                state.colorwheel.lastwidth = 1 - dx * state.colorwheel.width_incr;
                state.pen_width = 1/zoomlevel; }
            if(state.pen_width > state.tool_width_max / zoomlevel){
                state.colorwheel.lastwidth = state.tool_width_max - dx * state.colorwheel.width_incr;
                state.pen_width = state.tool_width_max/zoomlevel; } 
            state.status = "Width: " + Math.round(zoomlevel * state.pen_width) + (zoomlevel > 1? "/" + zoomlevel : "");
            state.status_expire = Date.now() + STATUS_TIMEOUT;
            window.requestAnimationFrame(function(){ refreshUI(ui, state); })}
    }
    if(!state.erase && state.colorwheel.dragmode == DRAG_VERTICAL){
        var dmodifier = -state.colorwheel.modifier_incr * state.colorwheel.offset[1];
        //if(state.pencil_enabled) dmodifier = -dmodifier;
        var value;
        var max = state.pencil_enabled? state.pencil_opacity_max : state.pen_brightness_max;
        var value_max = state.pencil_enabled? (max - 1)/max : 1;
        var min = state.pencil_enabled? 1/max : 0;
        function assignModifierValue(_value){
            value = _value;
            if(state.pencil_enabled){
                state.pencil_opacity = _value; }
            else{
                pen.brightness = _value; }}
        assignModifierValue(dmodifier + state.colorwheel.last_modifier_value);
        if(value < min){
            state.colorwheel.last_modifier_value = min + dy * state.colorwheel.modifier_incr;
            assignModifierValue(min); }
        if(value > value_max){
            state.colorwheel.last_modifier_value = value_max + dy * state.colorwheel.modifier_incr;
            assignModifierValue(value_max); }
        //console.log('max', max);
        state.status = (state.pencil_enabled? "Opacity" : "Brightness") + ": ";
        state.status += (max * value).toFixed(0) + (max == 100? "%" : "/" + max);
        state.status_expire = Date.now() + STATUS_TIMEOUT;
        window.requestAnimationFrame(function(){refreshUI(ui, state); })}
}
function mouseUp(ui,state, x,y){
    document.body.classList.remove("no_cursor");
    var delay = Date.now() - state.mousedowntime;
    var zoomvalue = state.zoom_display.getScale();
    if(state.scene_window_drag_start){
        state.scene_window_drag_start = null; }
    // tap joystick to move right half screen while zoomed.
    if(state.joystick.offset &&
       !state.joystick.dragging &&
       delay < LONG_PRESS_DELAY){
        joystickTap(ui,state);
    }
    //
    state.refresh = true;
    state.mousedown = null;
    state.last_mouse_pos = null;
    state.joystick.offset = null;
    state.joystick.dragging = false;
    if(state.colorwheel.offset && state.colorwheel.dragmode == DRAG_NONE
       && !state.erase && delay < LONG_PRESS_DELAY){
        // next pen
        state.status_expire = Date.now() + STATUS_TIMEOUT;
        state.pen_index = (state.pen_index + 1) % state.pen_list.length;
        var pen = state.pen_list[state.pen_index];
        var color = pen.color;
        if(state.pencil_enabled){
            color[3] = state.pencil_opacity; }
        else{
            color[3] = 1.0; }
        var hexcolor = (color[0] < 16? "0" : "") + color[0].toString(16) +
                       (color[1] < 16? "0" : "") + color[1].toString(16) +
                       (color[2] < 16? "0" : "") + color[2].toString(16);
        state.status = "Color #" + (state.pen_index + 1) + ": " + hexcolor;
        state.status_expire = Date.now() + STATUS_TIMEOUT;
        window.requestAnimationFrame(function(){refreshUI(ui, state); })}
    state.colorwheel.offset = null;
    state.colorwheel.dragmode = DRAG_NONE; }

})(exports);
