

function remove_app_from_cache(){
    return new Promise(function(resolve, reject){
        caches.delete('mycache').then(function(){ console.log('Cleared any previous cache.');
			localStorage.setItem("installed_version", "uninstalled");
            resolve.apply(null, arguments);
		}).catch(function(){ reject.apply(null, arguments); });
    })
}

function install_app_to_cache(){
    var cache_map = [ // url, cache_url
        //'./static/index.html', './app/index.html',
        //'./static/test.html', './app/test.html',
        '../index.html', '..',
        '../index.html', '../',
        '../index.html', '../index.html',
        '../static/index.html', '../app/',
        '../static/index.html', '../app/index.html',
        '../static/help.html', '../app/help.html',
        '../static/icon.png', '../app/icon.png',
        '../static/filesaver.min.js', '../app/filesaver.min.js',
        '../static/display2d.js', '../app/display2d.js',
        '../static/draw15.css', '../app/draw15.css',
        '../static/icon.png', '../app/icon.png',
        '../static/lisp_markup.js', '../app/lisp_markup.js',
        '../app/manifest.json', '../app/manifest.json',
        '../static/menu.css', '../app/menu.css',
        '../static/modal.css', '../app/modal.css',
        '../static/sketch.js', '../app/sketch.js',
        '../static/sketch_animate.js', '../app/sketch_animate.js',
        '../static/sketch_buttons.js', '../app/sketch_buttons.js',
        '../static/sketch_color.js', '../app/sketch_color.js',
        '../static/sketch_display.js', '../app/sketch_display.js',
        '../static/sketch_drawing.js', '../app/sketch_drawing.js',
        '../static/sketch_init.js', '../app/sketch_init.js',
        '../static/sketch_menus.js', '../app/sketch_menus.js',
        '../static/sketch_menu_actions.js', '../app/sketch_menu_actions.js',
        '../static/sketch_page.js', '../app/sketch_page.js',
        '../static/sketch_saveload.js', '../app/sketch_saveload.js',
        '../static/sketch_toolinput.js', '../app/sketch_toolinput.js',
        '../static/sketch_geometry.js', '../app/sketch_geometry.js',
        '../static/sketch_vector.js', '../app/sketch_vector.js',
        '../static/storage_chrome.js', '../app/storage_chrome.js',
        '../static/storage_chrome_local_shim.js', '../app/storage_chrome_local_shim.js',
    ]
    var cache_list = [];
    return caches.delete('mycache').then(function(){
        console.log('Cleared any previous cache.');
        return caches.open('mycache').then(function(cache){
            var promises = [];
            for(var i=0; i<cache_map.length; i+=2){
                let url = cache_map[i];
                let cache_url = cache_map[i+1];
                console.log(`Saving ${url} as ${cache_url}`);
                promises.push(fetch(url).then(function(response){
                    return cache.put(cache_url, response); }))}
            if(cache_list.length) promises.push(cache.addAll(cache_list));
            console.log(`Saving urls: ${cache_list.join(', ')}`);
            return new Promise(function(resolve, reject){
                Promise.all(promises).then(function(){
                    localStorage.setItem("installed_version", AVAILABLE_VERSION);
                    resolve.apply(null, arguments); })
                .catch(function(){
                    localStorage.removeItem("installed_version");
                    reject.apply(null, arguments); })})
    })})
}

