



function sw_unregister(){
    if('serviceWorker' in navigator){
        navigator.serviceWorker.getRegistrations().then(function(registrations) {
            var tasks = [];
			for(let registration of registrations) {
				tasks.push(registration.unregister()); }
            return Promise.all(tasks);
        })
    }
}

function sw_register(){
	var app_scope_path = "../";
    if('serviceWorker' in navigator){
        return navigator.serviceWorker.register("../service_worker.js", {scope: app_scope_path});
    }
}

