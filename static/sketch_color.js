

var exports = window.SketchColor = {};
(function(exports){

exports.getShadedColor = getShadedColor;
exports.getStateColor = getStateColor;
exports.getStateColorOpaque = getStateColorOpaque;
exports.getStateBorderColor = getStateBorderColor;

function getShadedColor( color,brightness,opacity){
    var has_opacity = opacity !== null && opacity !== undefined;
    if(color.length == 3){
        var result = has_opacity? [0,0,0, opacity] : [0,0,0]; }
    else if(color.length == 4){
        var result = [0,0,0, has_opacity? opacity : color[3]]; }
    else throw("SketchColor.getShadedColor color was not array of length 3 or 4");
    if(color[0] == 0 && color[1] == 0 && color[2] == 0){
        brightness = brightness/2 + 0.5; }
    else if(color[0] == 255 && color[1] == 255 && color[2] == 255){
        brightness /= 2; }
    if(brightness < 0.5){
        result[0] = Math.floor(color[0]*2*brightness);
        result[1] = Math.floor(color[1]*2*brightness);
        result[2] = Math.floor(color[2]*2*brightness); }
    else{
        result[0] = Math.floor(255 - (255-color[0])*2*(1-brightness));
        result[1] = Math.floor(255 - (255-color[1])*2*(1-brightness));
        result[2] = Math.floor(255 - (255-color[2])*2*(1-brightness)); }
    return result;
}

function getStateColorOpaque( state, index, ignore_brightness){
    if(index === "undefined" || index == null){
        index = state.pen_index; }
    var pen = state.pen_list[index];
    var color = ignore_brightness? pen.color :
                    Color.getShadedColor(pen.color,pen.brightness);
    if(color.length == 4){
        color = color.slice(0,3); }
    return "rgb(" + color.join(",") + ")";
}
function getStateColor( state, index){
    if(index === "undefined" || index == null){
        index = state.pen_index; }
    var pen = state.pen_list[index];
    var opacity = state.pencil_enabled? state.pencil_opacity : null;
    var color = Color.getShadedColor(pen.color, pen.brightness, opacity);
    var e = 0.0001;
    if(color.length == 3){
        return "rgb(" + color.join(",") + ")"; }
    else if(color.length == 4){
        return "rgba(" + color.join(",") + ")"; }
}

// different brightness from normal pen color for some contrast, but same hue
// to help indicate normal pen color when pen is small.
function getStateBorderColor( state, index){
    if(index === "undefined" || index == null){
        index = state.pen_index; }
    var pen = state.pen_list[index];
    var color = pen.color;
    var brightness = pen.brightness;
    if(brightness > 0.70){
        brightness -= 0.25; }
    else{
        brightness += 0.30; }
    var color = Color.getShadedColor( color,brightness);
    if(color.length == 4){
        color = color.slice(0,3); }
    // always opaque version.
    return "rgb(" + color.join(",") + ")";
}


})(exports);
