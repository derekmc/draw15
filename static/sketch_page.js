
var version = localStorage.getItem("installed_version");
var new_version = false;
if(typeof AVAILABLE_VERSION != 'undefined' && version != "uninstalled" && version != AVAILABLE_VERSION){
    new_version = true;}

var page_src = [
  ['#overlay', {style:"visibility:hidden"}],
  ['canvas#canvas'],
  ['#status', ""],
  ['#banner_outer',
    ['#banner.noselect', 
      ['#banner_text',
       ''],
      ['button#banner_button', 'Scene']]],
  ['#nav.noselect',
    ['button#menulink.noselect' + (new_version? '.update_available' : ''), '=']],
  ['#bottom', [
    ['#joystick'],
    ['canvas#colorwheel', {width: 75, height: 75}],
    ['#tools.noselect',
      ['div', {style:"display: inline-block;"},
          ['button#toolcycle.selected', 'Pen'], // cycle: pen, pencil
          ['button#erasebutton', 'x'],
          ['button#linebutton', 'S'],
      ],
      ['div', {style:"display: inline-block;"},
        ['button#undo', 'Undo'],
        ['button#redo', 'Redo'],
        ['button#zoomin', '+'],
        ['button#zoomout', '-']],
    ]]]];
      
document.body.innerHTML = LispMarkup.toHtml(page_src);
