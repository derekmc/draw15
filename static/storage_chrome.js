(function(exports){
	exports.get = function(k, next){
	    chrome.storage.local.get(k,next); }
	exports.set = function(items, next){
	    chrome.storage.local.set(items, next); }
	exports.remove = function(k, next){
		chrome.storage.local.remove(k, next); }
})(typeof exports === 'undefined'? this['Storage']={}: exports);
