var exports = window.SketchDisplay = {};

(function(exports){
var GHOST_WEIGHT_ZOOMED = 0.1;
var GHOST_WEIGHT_FULLSCALE = 0.35;
var CURSOR_SIZE = 10;

// display drawing functions
exports.drawLastStroke = drawLastStroke;
exports.drawSceneImage = drawSceneImage;
exports.drawCursor = drawCursor;
exports.drawZoomedImage = drawZoomedImage;
exports.refreshAll = refreshAll;
exports.refreshUI = refreshUI;
exports.sceneFadeAnimate = sceneFadeAnimate;

var canvasToScreen = SketchGeometry.canvasToScreen;
var canvasToScreenZoomed = SketchGeometry.canvasToScreenZoomed;
var screenToCanvas = SketchGeometry.screenToCanvas;
var screenZoomedToCanvas = SketchGeometry.screenZoomedToCanvas;
var zoomWindowBounds = SketchGeometry.zoomWindowBounds;

function drawCursor(ui, state){
    var size = 60;
    var ctx = ui.canvas.getContext("2d");
    // TODO get cursor position and draw it.

    if(!state.last_mouse_pos){
        return; }
    var pos = state.last_mouse_pos.slice(0);
    screenZoomedToCanvas(pos, state, pos);
    canvasToScreen(pos, state, pos);
    var x = pos[0];
    var y = pos[1];

    var x0 = x - size/2;
    var x1 = x - 2 * size/5;
    var x2 = x + 2 * size/5;
    var x3 = x + size/2;

    var y0 = y - size/2;
    var y1 = y - 2 * size/5;
    var y2 = y + 2 * size/5;
    var y3 = y + size/2;

    ctx.save();
    //ctx.globalCompositeOperation = "xor";

    // background
    ctx.beginPath();
    ctx.lineWidth = 4;
    ctx.strokeStyle = "#fff";
    ctx.moveTo(x, y0);
    ctx.lineTo(x, y1);
    ctx.moveTo(x, y2);
    ctx.lineTo(x, y3);
    ctx.moveTo(x0, y);
    ctx.lineTo(x1, y);
    ctx.moveTo(x2, y);
    ctx.lineTo(x3, y);
    ctx.stroke();

    // foreground
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = "#000";
    ctx.moveTo(x, y0);
    ctx.lineTo(x, y1);
    ctx.moveTo(x, y2);
    ctx.lineTo(x, y3);
    ctx.moveTo(x0, y);
    ctx.lineTo(x1, y);
    ctx.moveTo(x2, y);
    ctx.lineTo(x3, y);
    ctx.stroke();
    ctx.restore();
}
function drawLastZoomedStroke(ui, state,ghosted){
    var ctx = ui.canvas.getContext("2d");
    var zoomvalue = state.zoom_display.getScale();
    // draw actual size lines.
    var pt = [0,0]; // reuse;
    var pos = [0,0]; 
    var d = state.drawing;
    var arr = d.points;
    var grain = d.point_grain;
    var drawfore = state.drawfore;
    if(drawfore){
        var index = d.strokes.length-1; }
    else{
        var index = 0; }

    if(d.strokes.length){
        var stroke = d.strokes[index];
        if(stroke.length < 4){
            console.warn("Drawing strokes should have at least 4 entries:" + JSON.stringify(stroke));
            return; }
        var style = stroke[0];
        var width = stroke[1];
        var point_count = stroke[3] - stroke[2] + 1;
        ctx.beginPath();
        if(ghosted){
            if(!style.match(/^rgb(a){0,1}\([0-9]+,[0-9]+,[0-9]+(,[0-9.]+){0,1}\)$/)){
                console.warn("Did not recognize style color: '" + style + "'"); }
            else{
                var c = [0,0,0,1.0];
                var i0 = style.indexOf("(")+1;
                var i1 = style.indexOf(")");
                var _c = style.substring(i0,i1).split(",");
                c[0] = parseInt(_c[0]);
                c[1] = parseInt(_c[1]);
                c[2] = parseInt(_c[2]);
                if(_c.length == 3) _c[3] = 1.0;
                else c[3] = parseFloat(_c[3]);
                c[3] *= 1 - state.scene_visibility / state.scene_visibility_max;
                ctx.strokeStyle = "rgba(" + c.join(",") + ")"; }}
        else{
            ctx.strokeStyle = style; }
        ctx.lineWidth = zoomvalue * stroke[1];
        // TODO parameterized end segments (because erase tool can remove partial segments)
        for(var j=0; j<point_count; ++j){
            d.strokePoint(pt, stroke, j);
            canvasToScreenZoomed(pos, state,pt);
            if(point_count == 1){  // single point;
                ctx.moveTo(pos[0], pos[1]);
                ctx.lineTo(pos[0]+0.01, pos[1]); }
            else if(j==0) ctx.moveTo(pos[0], pos[1]);
            else{ ctx.lineTo(pos[0], pos[1]); }}
       ctx.stroke(); }
    if(ghosted){
        var zoomfactor = state.zoom_display.getScale() / state.scene_display.getScale()
        var w = canvas.width;
        var h = canvas.height;
        var zoom_window = SketchGeometry.zoomWindowBounds(state);
        var x = zoom_window[0];
        var y = zoom_window[1];
        var dx = zoom_window[2] - x;
        var dy = zoom_window[3] - y; }
        //ctx.drawImage(background, x,y,dx,dy, x,y,dx,dy); }
}
// uses ui.canvas
function drawLastFullscaleStroke(canvas, state,ghosted){
    var ctx = canvas.getContext("2d");
    var zoomvalue = state.zoom_display.getScale();
    // draw actual size lines.
    var pt = [0,0]; // reuse;
    var pos = [0,0]; 
    var d = state.drawing;
    var arr = d.points;
    var grain = d.point_grain;
    var drawfore = state.drawfore;
    if(drawfore){
        var index = d.strokes.length-1; }
    else{
        var index = 0; }

    if(d.strokes.length){
        var stroke = d.strokes[index];
        if(stroke.length < 4){
            console.warn("Drawing strokes should have at least 4 entries:" + JSON.stringify(stroke));
            return; }
        var style = stroke[0];
        var width = stroke[1];
        var point_count = stroke[3] - stroke[2] + 1;
        ctx.beginPath();
        if(ghosted){
            ctx.strokeStyle = "rgba(64,64,64,0.333)"; }
        else{
            ctx.strokeStyle = style; }
        ctx.lineWidth = (zoomvalue < state.scene_display.getScale())? zoomvalue * stroke[1]: state.scene_display.getScale() * stroke[1];
        // TODO parameterized end segments (because erase tool can remove partial segments)
        if(stroke.length != 4){
            console.warn("Drawing strokes should have 4 entries, got " + stroke.length);
            return; }

        for(var j=0; j<point_count; ++j){
            d.strokePoint(pt, stroke, j);
            canvasToScreen(pos, state,pt);
            if(point_count == 1){  // single point;
                ctx.moveTo(pos[0], pos[1]);
                ctx.lineTo(pos[0]+0.01, pos[1]); }
            else if(j==0) ctx.moveTo(pos[0], pos[1]);
            else{ ctx.lineTo(pos[0], pos[1]); }}

        ctx.stroke(); }
}
// uses ui.canvas, ui.foreground, ui.background
function drawLastStroke(canvas,background,foreground, state){
    // draw actual size lines.
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
    var drawfore = state.drawfore;
    //var drawfore = state.drawfore;

    if(state.show_scene){
        var w = canvas.width;
        var h = canvas.height;
        // draw zoombox outline
        var zoom_window = SketchGeometry.zoomWindowBounds(state);
        var x = zoom_window[0];
        var y = zoom_window[1];
        var dx = zoom_window[2] - x;
        var dy = zoom_window[3] - y; 
        if(drawfore){
            ctx.drawImage(background,0,0);
            //var cover_alpha = 1 - state.scene_visibility/state.scene_visibility_max;
            var cover_alpha = 0.50;
            //drawZoomBox(canvas, state,false);
            var overlay_alpha = 1 - state.scene_visibility / state.scene_visibility_max;; 
            ctx.globalAlpha = overlay_alpha;
            ctx.drawImage(foreground,0,0);
            ctx.globalAlpha = 1.0;
            drawLastZoomedStroke(canvas, state,true);
            //ctx.fillStyle = "rgba(255,255,255,0.70)";
            //ctx.fillRect(x,y,dx,dy);
            drawLastFullscaleStroke(canvas, state,false); }
        else{
            drawLastFullscaleStroke(canvas, state,false);
            ctx.drawImage(background,0,0);
            //var cover_alpha = 1 - state.scene_visibility/state.scene_visibility_max;
            var cover_alpha = 0.50;
            drawZoomBox(ui.canvas, state,false);
            drawLastZoomedStroke(ui, state,true);
            var overlay_alpha = 1 - state.scene_visibility / state.scene_visibility_max;; 
            ctx.globalAlpha = overlay_alpha;
            ctx.drawImage(ui.foreground,0,0);
            ctx.globalAlpha = 1.0; }
        drawCursor(ui, state);
    }
    else{
        if(drawfore){
            ctx.drawImage(ui.foreground,0,0);
            drawLastZoomedStroke(ui, state,false); }
        else{
            drawLastZoomedStroke(ui, state,false);
            ctx.drawImage(ui.foreground,0,0); }
    }
}


// TODO ui parameter is not being passed explicitly
function drawZoomBox(canvas, state,ghosted, cover_alpha){
    var ctx = canvas.getContext("2d");
    var zoomfactor = state.zoom_display.getScale() / state.scene_display.getScale();
    var w = canvas.width;
    var h = canvas.height;
    var int = Math.floor;
    // draw zoombox outline
    if(!state.hide_ui){
        ctx.save();
        var zoom_window = SketchGeometry.zoomWindowBounds(state);
        var x = int(zoom_window[0]) + 0.5;
        var y = int(zoom_window[1]) + 0.5;
        var dx = int(zoom_window[2] - x);
        var dy = int(zoom_window[3] - y); 
        ctx.globalCompositeOperation = "source-over";
        if(cover_alpha){
            ctx.fillStyle = "rgba(200,200,200," + cover_alpha + ")";
            ctx.fillRect(0,0,w+1,y+1);
            ctx.fillRect(0,y,x,dy+1);
            ctx.fillRect(x+dx,y,w-dx-x,dy+1);
            ctx.fillRect(0,y+dy,w,h-y-dy); 
        }
        if(ghosted){
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.rect(x,y,dx,dy);
            ctx.setLineDash([10]);
            ctx.strokeStyle = "rgba(0,0,0,0.5)";
            ctx.stroke();
        }
        else{
            ctx.beginPath();
            ctx.rect(x,y,dx,dy);
            ctx.lineWidth = 2;
            ctx.strokeStyle = "white";
            ctx.stroke();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "black";
            ctx.setLineDash([5]);
            ctx.stroke(); }
        var grab_width = state.ZOOM_GRAB_WIDTH;
        ctx.fillStyle = "#fff";
        ctx.fillRect(x - grab_width/2, y - grab_width/2, grab_width, grab_width);
        ctx.strokeStyle = "#000";
        ctx.setLineDash([]);
        ctx.lineWidth = 1;
        ctx.strokeRect(x - grab_width/2, y - grab_width/2, grab_width, grab_width);
        ctx.restore();
    }
}
function drawSceneImage(canvas,buffer,state, ghosted){
    state.scene_display.getSpaceFocus(state.scene_redraw_focus);
    var zoomvalue = state.zoom_display.getScale();
    // draw actual size lines.
    var lastwidth = 0;
    var laststyle = null;
    var pt = [0,0]; // reuse;
    var pos = [0,0]; 
    var d = state.drawing;
    var strokes = d.strokes;
    var arr = d.points;
    var grain = d.point_grain;
    var screen_origin = state.scene_display.displayToSpace(null, [0,0]);
    var screen_bottomright = state.scene_display.displayToSpace(null, [canvas.width, canvas.height]);

    var draw_params = []; // [{ ctx, buffer, bg, style, outline, composite, alpha }]
    var ctx = canvas.getContext("2d");
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    if(ghosted){
        /*
        draw_params.push({ctx: ctx,
                         buffer: buffer,
                         background: null,
                         style: "rgba(255,255,255,0.667)",
                         outline: 1.0,
                         composite:"screen",
                         alpha: 1.0});
        draw_params.push({ctx:ctx,
                         buffer: buffer,
                         background: null,//"#fff",
                         style: "rgba(0,0,0,0.667)",
                         outline: 0,
                         composite: "multiply",
                         alpha: 1.0});
                         */
        draw_params.push({ctx: ctx,
                         buffer: null,
                         background: null,
                         outline: 0,
                         composite: null
                         });
    }
    else{
        draw_params.push({ctx: ctx,
                         buffer: null,
                         background: null,
                         style: null,
                         outline: 0,
                         composite: null
                         }); } // null means "source-over"
    
    for(var k = 0; k<draw_params.length; ++k){
        var p = draw_params[k];
        var ctx = p.ctx;
        if(p.buffer){
            ctx = p.buffer.getContext("2d");
            ctx.lineCap = "round";
            ctx.lineJoin = "round";
            if(p.background){
                ctx.fillStyle = p.background;
                ctx.fillRect(0,0,ui.canvas.width,ui.canvas.height); }
            else{
                ctx.clearRect(0,0,ui.canvas.width,ui.canvas.height); }}
 
        ctx.strokeStyle = "rgb(128,0,0)";
        for(var i=0; i<strokes.length; ++i){
            var stroke = strokes[i];
            var point_count = stroke[3] - stroke[2] + 1;
            var style = stroke[0];
            var c = [0,0,0,1.0];
            if(!style.match(/^rgb(a){0,1}\([0-9]+,[0-9]+,[0-9]+(,[0-9.]+){0,1}\)$/)){
                console.warn("Did not recognize style color: '" + style + "'"); }
            else{
                var i0 = style.indexOf("(")+1;
                var i1 = style.indexOf(")");
                var _c = style.substring(i0,i1).split(",");
                c[0] = parseInt(_c[0]);
                c[1] = parseInt(_c[1]);
                c[2] = parseInt(_c[2]);
                if(_c.length == 3) _c[3] = 1.0;
                else c[3] = parseFloat(_c[3]); }
            var line_width = stroke[1];
            var is_transparent = (c.length == 4 && 0.005 < Math.abs(1.0 - c[3]));
            var linechanged = (i == 0 || style != laststyle || line_width != lastwidth);
            if(is_transparent || linechanged){
                if(i>0) ctx.stroke();
                ctx.beginPath();
                ctx.stokeStyle = style;
                if(p.style){
                    if(typeof p.style == "function"){
                        ctx.strokeStyle = p.style(c[0], c[1], c[2], c[3]); }
                    else{
                        ctx.strokeStyle = p.style; }}
                else{
                    ctx.strokeStyle = style; }
                ctx.lineWidth = 2*p.outline + line_width * ((zoomvalue < state.scene_display.getScale())? zoomvalue : state.scene_display.getScale());
                lastwidth = line_width;
                laststyle = style; }
            // TODO parameterized end segments (because erase tool can remove partial segments)
            if(stroke.length != 4){
                console.warn("Drawing strokes should have 4 entries, got " + stroke.length);
                return; }
            for(var j=0; j<point_count; ++j){
                d.strokePoint(pt, stroke, j);
                canvasToScreen(pos, state,pt);
                if(point_count == 1){  // single point;
                    ctx.moveTo(pos[0], pos[1]);
                    ctx.lineTo(pos[0]+0.01, pos[1]); }
                else if(j==0) ctx.moveTo(pos[0], pos[1]);
                else{ ctx.lineTo(pos[0], pos[1]); }}}

        if(strokes.length) ctx.stroke();
        if(p.buffer){
            ctx = p.ctx;
            if(p.composite){
                ctx.globalCompositeOperation = p.composite; }
            if(p.alpha){
                ctx.globalAlpha = p.alpha; }
            else{
                ctx.globalAlpha = 0; }
            ctx.drawImage(p.buffer,0,0);
            // restore defaults
            ctx.globalCompositeOperation = "source-over";
            ctx.globalAlpha = 1.0;
        }
    }
}
function drawZoomedImage(canvas,buffer,state, outline_only,ghosted){
    state.zoom_display.getSpaceFocus(state.zoom_redraw_focus);
    var zoomvalue = state.zoom_display.getScale();
    // draw actual size lines.
    var lastwidth = 0;
    var laststyle = null;
    var pt = [0,0]; // reuse;
    var pos = [0,0]; 
    var d = state.drawing;
    var strokes = d.strokes;
    var arr = d.points;
    var grain = d.point_grain;
    var w = canvas.width;
    var h = canvas.height;

    var draw_params = []; // [{ ctx, buffer, bg, style, outline, composite, alpha }]
    var ctx = canvas.getContext("2d");
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    var scene_visibility = state.scene_visibility / state.scene_visibility_max;
                         
    if(outline_only){
        draw_params.push({ctx: ctx,
                         buffer: buffer,
                         background: null,
                         style: "rgba(255,255,255,0.75)",
                         outline: 2.0,
                         composite:"screen",
                         alpha: 1.0});
    }
    else if(ghosted){
        draw_params.push({ctx:ctx,
                         buffer: buffer,
                         background: null,//"#fff",
                         style: "rgba(255,255,255,1.0)",
                         outline: 4.0,
                         composite:null,
                         alpha: 1.0});

        draw_params.push({ctx:ctx,
                         buffer: buffer,
                         background: null,//"#fff",
                         style: null,
                         outline: 0,
                         composite:null,
                         alpha: 1.0});
    }
    else{
        draw_params.push({ctx:ctx,
                         buffer: buffer,
                         background: null,//"#fff",
                         style: null,
                         outline: 0,
                         composite:null,
                         alpha: 1.0});
    }
    
    for(var k = 0; k<draw_params.length; ++k){
        var p = draw_params[k];
        var ctx = p.ctx;
        if(p.buffer){
            ctx = p.buffer.getContext("2d");
            ctx.lineCap = "round";
            ctx.lineJoin = "round";
            if(p.background){
                ctx.fillStyle = p.background;
                ctx.fillRect(0,0,canvas.width,canvas.height); }
            else{
                ctx.clearRect(0,0,canvas.width,canvas.height); }}
        

        for(var i=0; i<strokes.length; ++i){
            var stroke = strokes[i];
            var style = stroke[0];
            var line_width = stroke[1];
            var point_count = stroke[3] - stroke[2] + 1;
            var is_transparent = (style.indexOf("rgba") == 0 && 0.0001 < Math.abs(1.0 - style.substring(5,style.length-1).split(",")[3]));
            var linechanged = (i == 0 || style != laststyle || line_width != lastwidth);
            var c = [0,0,0,1.0];
            if(!style.match(/^rgb(a){0,1}\([0-9]+,[0-9]+,[0-9]+(,[0-9.]+){0,1}\)$/)){
                console.warn("Did not recognize style color: '" + style + "'"); }
            else{
                var i0 = style.indexOf("(")+1;
                var i1 = style.indexOf(")");
                var _c = style.substring(i0,i1).split(",");
                c[0] = parseInt(_c[0]);
                c[1] = parseInt(_c[1]);
                c[2] = parseInt(_c[2]);
                if(_c.length == 3) _c[3] = 1.0;
                else c[3] = parseFloat(_c[3]); }

            /*
            if(is_transparent || linechanged){
                if(i>0) ctx.stroke();
                ctx.beginPath();
                ctx.stokeStyle = style;
                if(p.style){
                    if(typeof p.style == "function"){
                        ctx.strokeStyle = p.style(c[0], c[1], c[2], c[3]); }
                    else{
                        ctx.strokeStyle = p.style; }}
                else{
                    ctx.strokeStyle = style; }
                ctx.lineWidth = (line_width + 2*p.outline) * ((zoomvalue < state.default_zoom)? zoomvalue : state.default_zoom);
                lastwidth = line_width;
                laststyle = style; }
                */

            if(is_transparent || linechanged){
                if(i>0) ctx.stroke();
                ctx.beginPath();
                if(p.style){
                    if(typeof p.style == "function"){
                        ctx.strokeStyle = p.style(c[0], c[1], c[2], c[3]); }
                    else{
                        ctx.strokeStyle = p.style; }}
                else{
                    ctx.strokeStyle = style; }

                ctx.strokeStyle = p.style? p.style : style;
                ctx.lineWidth = zoomvalue * line_width + 2 * p.outline;
                lastwidth = line_width;
                laststyle = style; }
            // TODO parameterized end segments (because erase tool can remove partial segments)
            if(stroke.length != 4){
                console.warn("Drawing strokes should have 4 entries, got " + stroke.length);
                return; }
        for(var j=0; j<point_count; ++j){
            d.strokePoint(pt, stroke, j);
            canvasToScreenZoomed(pos, state,pt);
            if(point_count == 1){  // single point;
                ctx.moveTo(pos[0], pos[1]);
                ctx.lineTo(pos[0]+0.01, pos[1]); }
            else if(j==0) ctx.moveTo(pos[0], pos[1]);
            else{ ctx.lineTo(pos[0], pos[1]); }}}
 
        if(strokes.length) ctx.stroke();
        if(p.buffer){
            ctx = p.ctx;
            if(p.composite){
                ctx.globalCompositeOperation = p.composite; }
            if(p.alpha){
                ctx.globalAlpha = p.alpha; }
            else{
                ctx.globalAlpha = 0; }
            ctx.drawImage(p.buffer,0,0);
            // restore defaults
            ctx.globalCompositeOperation = "source-over";
            ctx.globalAlpha = 1.0;
        }
    }
}



function refreshAll(ui, state, suppress_redraw){
    var redraw = !suppress_redraw;
    var w = ui.canvas.width;
    var h = ui.canvas.height;
    var ctx = ui.canvas.getContext("2d");
    var fgctx = ui.foreground.getContext("2d");
    var bgctx = ui.background.getContext("2d");
    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    var zoomvalue = state.zoom_display.getScale()
    var scene_zoomvalue = state.scene_display.getScale()
    //var emphasize_zoomed = !state.joystick.offset;
    //var show_scene = state.show_scene;
    var focus = [0,0];
    var offset_x = 0;
    var offset_y = 0;
    if(!redraw){
        if(state.show_scene && state.mousedown){
            state.scene_display.getSpaceFocus(focus);
            offset_x = -scene_zoomvalue * (focus[0] - state.scene_redraw_focus[0]);
            offset_y = -scene_zoomvalue * (focus[1] - state.scene_redraw_focus[1]); 
            ctx.fillStyle = "rgba(220,220,220,0.45)"; }
        else{
            state.zoom_display.getSpaceFocus(focus);
            offset_x = -zoomvalue * (focus[0] - state.zoom_redraw_focus[0]);
            offset_y = -zoomvalue * (focus[1] - state.zoom_redraw_focus[1]); 
            ctx.fillStyle = "rgba(192,192,192,0.45)"; }
        ctx.fillRect(0,0,w,h);
    }
    ctx.clearRect(offset_x, offset_y, w, h);
    if(state.joystick.offset || state.scene_window_drag_start){ // panning
        if(state.show_scene){
            if(redraw){
                bgctx.clearRect(0,0,w,h);
                fgctx.clearRect(0,0,w,h);
                drawZoomedImage(ui.foreground,ui.buffer, state,false);
                drawSceneImage(ui.background,ui.buffer, state,false);
            }
            ctx.drawImage(ui.background, offset_x,offset_y);
            drawZoomBox(ui.canvas, state,false);
        }
        else{
            if(redraw){
                fgctx.clearRect(0,0,w,h);
                drawZoomedImage(ui.foreground,ui.buffer, state,false);
            }
            ctx.drawImage(ui.foreground, offset_x, offset_y);
        }
    }
    else if(state.hide_ui){
        if(redraw){
            fgctx.clearRect(0,0,w,h);
            drawZoomedImage(ui.foreground,ui.buffer, state,false); }
        ctx.drawImage(ui.foreground, offset_x, offset_y);
    }
    else if(state.show_scene){
        if(state.mousedown){ // only show scene
            if(redraw){ 
                bgctx.clearRect(0,0,w,h);
                fgctx.clearRect(0,0,w,h);
                drawZoomedImage(ui.foreground,ui.buffer, state,false);
                drawSceneImage(ui.background,ui.buffer, state,false);
            }
            ctx.clearRect(0,0,ui.canvas.width, ui.canvas.height);
            ctx.drawImage(ui.background,offset_x,offset_y);
            drawZoomBox(ui.canvas, state,false);
        }
        else{ // show both.
            //ctx.drawImage(ui.foreground, offset_x, offset_y);
            if(redraw){ 
                bgctx.clearRect(0,0,w,h);
                fgctx.clearRect(0,0,w,h);
                drawSceneImage(ui.background,ui.buffer, state,false);
                drawZoomedImage(ui.foreground,ui.buffer, state,false);
                /*
                var zoom_window = SketchGeometry.zoomWindowBounds(state);
                var x = zoom_window[0];
                var y = zoom_window[1];
                var dx = zoom_window[2] - x;
                var dy = zoom_window[3] - y; 
                fgctx.clearRect(x, y, dx, dy);*/
            }
 
            ctx.drawImage(ui.background,0,0);
            var cover_alpha = 0.50;
            ctx.fillStyle = "rgba(255,255,255,0.50)";
            ctx.fillRect(0,0,ui.canvas.width, ui.canvas.height);
            var overlay_alpha = 1 - state.scene_visibility / state.scene_visibility_max;; 
            ctx.globalAlpha = 1.0;
            if(!redraw){ 
                state.zoom_display.getSpaceFocus(focus);
                offset_x = -zoomvalue * (focus[0] - state.zoom_redraw_focus[0]);
                offset_y = -zoomvalue * (focus[1] - state.zoom_redraw_focus[1]);
            }
            drawZoomedImage(ui.canvas,null, state,true);
            drawZoomBox(ui.canvas, state,false);
            ctx.drawImage(ui.foreground,offset_x,offset_y);

            var zoom_window = SketchGeometry.zoomWindowBounds(state);
            var x = zoom_window[0];
            var y = zoom_window[1];
            var dx = zoom_window[2] - x;
            var dy = zoom_window[3] - y; 

            var grab_width = state.ZOOM_GRAB_WIDTH;
            ctx.save()
            ctx.fillStyle = "#fff";
            ctx.fillRect(x - grab_width/2, y - grab_width/2, grab_width, grab_width);
            ctx.strokeStyle = "#000";
            ctx.setLineDash([]);
            ctx.lineWidth = 1;
            ctx.strokeRect(x - grab_width/2, y - grab_width/2, grab_width, grab_width);
            ctx.restore(); 
        }
    }
    else{
        if(redraw){
            fgctx.clearRect(0,0,w,h);
            drawZoomedImage(ui.foreground,ui.buffer, state,false); }
        ctx.drawImage(ui.foreground, offset_x, offset_y);
    }
    /*
    else{
        ctx.fillStyle = "rgba(255,255,255,0.50)";
        ctx.fillRect(0,0,ui.canvas.width, ui.canvas.height);
        drawZoomedImage(ui.foreground, ui.buffer, state, false, false);
        ctx.drawImage(ui.foreground,0,0);
    }
    }

        if(redraw){
            bgctx.clearRect(0,0,w,h);
            fgctx.clearRect(0,0,w,h);
            drawSceneImage(ui.background,ui.buffer, state,true);
            drawZoomedImage(ui.foreground,ui.buffer, state,false, true);
        }
        //ctx.drawImage(ui.background,0,0);
        //var cover_alpha = 0.50;
        //var overlay_alpha = 1 - state.scene_visibility / state.scene_visibility_max;; 
        //ctx.globalAlpha = 1.0;
    }
    */
    refreshUI(ui, state);
    //if(!redraw) ui.status.innerHTML = offset_x + ", " + offset_y;
}

function setClass(elem, classname,value){
    if(value){
        elem.classList.add(classname); }
    else{
        elem.classList.remove(classname); }
}
function setEnabled(elem, value){
    elem.disabled = value? false : true;
}

// params: { penWidth, penIndex, penCount, colors, borderColors, borderWidth, startAngle } 

function drawPenControl(canvas, params){
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
    var pi = Math.PI;
    var sin = Math.sin;
    var cos = Math.cos;
    var border = params.borderWidth;
    var penWidth = params.penWidth;
    var r = params.size/2 - 1;
    var x0 = params.size/2;
    var y0 = params.size/2;
    var colors = params.colors;
    var opaqueColors = params.opaqueColors;
    var borderColors = params.borderColors;
    var count = params.penCount;
    var angle0 = params.startAngle;
    var penIndex = params.penIndex;
    
    if(params.drawBackground){
        ctx.beginPath();
        ctx.fillStyle = "#000";
        ctx.arc(x0, y0, r, 0, 2*Math.PI);
        ctx.fill();
    }
    //draw active pen last.
    for(var _i=penIndex+1; _i<penIndex + count + 1; ++_i){
        var i = _i%count;
        var angle1 = angle0 + i*2*Math.PI/count;
        var angle2 = angle0 + (i+1)*2*Math.PI/count;
        var midAngle = (angle1 + angle2)/2;
        var color1 = opaqueColors[i]? opaqueColors[i] : "#000";
        var color2 = borderColors[i]? borderColors[i]: "#fff";
   
        var _r = (i==penIndex? r-border+1 : r);
        if(params.drawBackground && i != penIndex)_r -= border;
        var _border = (i==penIndex? 2*border : 0);
       
        ctx.beginPath();
        var xoff = 0;//_border*cos(midAngle);
        var yoff = 0;//_border*sin(midAngle);
        ctx.moveTo(x0+xoff, y0+yoff);
        ctx.lineTo(x0+xoff+_r*cos(angle1), y0+yoff+_r*sin(angle1)); 
        ctx.arc(x0+xoff, y0+yoff, _r, angle1, angle2);

        ctx.lineTo(x0+xoff, y0+yoff);
        ctx.fillStyle = color1;
        if(_border){
            ctx.lineWidth = _border;
            ctx.strokeStyle = color2;
            ctx.stroke(); }
        ctx.fill();
     }
     ctx.beginPath();
     ctx.strokeStyle = borderColors[penIndex]? borderColors[penIndex]: "#fff";
     ctx.arc(x0,y0,penWidth/2+border/2,0,2*pi);
     ctx.lineWidth = border;
     ctx.stroke();
     ctx.beginPath();
     ctx.arc(x0,y0,penWidth/2,0,2*pi);
     ctx.fillStyle = "#fff";
     ctx.fill();
     ctx.beginPath();
     ctx.moveTo(x0, y0);
     ctx.lineTo(x0 + penWidth/2, y0); 
     ctx.arc(x0,y0,penWidth/2,0,pi/2);
     ctx.closePath();
     ctx.fillStyle = "#000";
     ctx.fill();
     ctx.beginPath();
     ctx.moveTo(x0, y0);
     ctx.lineTo(x0-penWidth/2, y0); 
     ctx.arc(x0,y0,penWidth/2,pi,3*pi/2);
     ctx.closePath();
     ctx.fillStyle = "#000";
     ctx.fill();
     ctx.beginPath();
     ctx.arc(x0,y0,penWidth/2,0,2*pi);
     ctx.fillStyle = colors[penIndex]? colors[penIndex]: "#000";
     ctx.fill();
}

function zoomText(zoom){
    if(zoom < 1) zoom = "1/" + Math.round(1/zoom);
    else zoom = Math.round(10 * zoom)/10;
    return "x" + zoom;
}
function defaultStatus(state){
    
    var zoom = state.show_scene? state.scene_display.getScale() :
                                 state.zoom_display.getScale();
	return zoomText(zoom);
}
function sceneFadeAnimate(ui, state,phase){
    var a = 1 - state.scene_visibility / state.scene_visibility_max;
    var x = Math.sin(Math.PI * phase / 2);
    var alpha_fore =  a + (1-a)*x;
    var cover_alpha = a-a*x;
    var cover_color = "rgba(200,200,200," + (x) + ")";

    var ctx = ui.canvas.getContext("2d");
    ctx.globalAlpha = 1.0;
    ctx.clearRect(0,0,ui.canvas.width,ui.canvas.height);
    ctx.drawImage(ui.background,0,0);
    drawZoomBox(ui.canvas, state, true,0.15);
    ctx.fillStyle = cover_color;
    ctx.fillRect(0,0,ui.canvas.width,ui.canvas.height);
    //drawZoomBox(ui.canvas, state, true,cover_alpha);
    ctx.globalAlpha = alpha_fore;
    ctx.drawImage(ui.foreground,0,0);
    ctx.globalAlpha = 1.0;
}



function refreshUI(ui, state){      

    // hide ui, used by page->view action in menu.
    if(state.hide_ui){
        ui.banner.style.visibility = "hidden";
        ui.status.style.visibility = "hidden";
        ui.banner.style.visibility = "hidden";
        ui.nav.style.visibility = "hidden";
        ui.colorwheel.style.visibility = "hidden";
        ui.bottom.style.visibility = "hidden";
        ui.joystick.style.visibility = "hidden";
        ui.zoomin.style.display = "inline-block";
        ui.zoomout.style.display = "inline-block";
        return; }
    else if(state.show_scene){
        ui.status_text = defaultStatus(state);
        ui.banner.style.visibility = "visible";
        var zoom_scale = state.zoom_display.getScale();
        ui.banner_text.classList.remove("banner_text_hide");
		ui.banner_text.innerHTML = "Scene  &nbsp;" + zoomText(zoom_scale);
        ui.banner_button.innerHTML = "x";
        ui.status.style.visibility = "visible";
        ui.nav.style.visibility = "visible";
        ui.colorwheel.style.visibility = "visible";
        ui.bottom.style.visibility = "visible";
        //ui.tools.style.display = 'none';
        ui.joystick.style.visibility = "visible"; }
    else{
        ui.status.style.visibility = "visible";
        ui.banner_button.innerHTML = "Scene";
        ui.banner_text.innerHTML = "";
        ui.banner_text.classList.add("banner_text_hide");
        ui.banner.style.visibility = "visible";
        ui.nav.style.visibility = "visible";
        ui.colorwheel.style.visibility = "visible";
        ui.bottom.style.visibility = "visible";
        ui.zoomin.style.display = "inline-block";
        ui.zoomout.style.display = "inline-block";
        ui.joystick.style.visibility = "visible"; }
  
    // status
    while(ui.status.firstChild){
        ui.status.removeChild(ui.status.firstChild); }
    if(state.status && state.status.length){
        ui.status.style.visibility = "visible"
        ui.status.appendChild(document.createTextNode(state.status)); }
    else if(!state.scene_zoom){
        ui.status.appendChild(document.createTextNode(defaultStatus(state)));
        ui.status.style.visibility = 'visible'; }

      
    // linebutton, tool cycle, tool adjust
    ui.linebutton.innerHTML = state.draw_line_enabled? "Line" : "Curve";
    var tool = state.pencil_enabled? "Pencil" : "Pen";
    if(state.erase) tool = "Remove";
    ui.toolcycle.innerHTML = tool;

    var pen = state.pen_list[state.pen_index];
    var zoomvalue = state.zoom_display.getScale();
    //ui.zoomtoggle.innerHTML = " x" + (0.01*Math.round(zoomvalue*100));
    //setClass(ui.zoomtoggle, "show_scene", state.show_scene && zoomvalue > 1);
    //setClass(ui.zoomtoggle, "no_toggle", zoomvalue <= 1);

    setClass(ui.erasebutton, "selected", state.erase);
    setClass(ui.toolcycle, "selected", !state.erase);
    //setClass(ui.toolcycle, "highlight", tool == "Remove");
    setClass(ui.toolcycle, "background", !state.drawfore && !state.erase);
    setClass(ui.zoomin, "background", state.show_scene);
    setClass(ui.zoomout, "background", state.show_scene);
    if(state.erase){
        var margin = 2;
        var ctx = ui.colorwheel.getContext("2d");
        ctx.clearRect(0,0,ui.colorwheel.width, ui.colorwheel.height);
        var style = ui.colorwheel.style;
        //style.visibility = 'hidden';
        style.borderWidth = 0;
        style.width = 75;
        style.height = 75;
        style.backgroundColor = 'rgba(255,255,255,0.0)';
        var side = ui.colorwheel.width - 2 * margin;
        ctx.fillStyle= 'rgba(255,255,255,0.85)';
        ctx.strokeStyle="black";
        ctx.lineWidth=3;

        // outline
        ctx.beginPath();
        ctx.arc(side/2 + margin,side/2 + margin, side/2 - 2 - margin, 0,2*Math.PI);
        ctx.stroke();
        ctx.fill();

        // eraser
        ctx.beginPath();
        var max_width = state.tool_width_max;
        var width = Math.max(1, Math.min(max_width, Math.round(zoomvalue * state.remove_width)));
        var r = width/2 + 1;
        ctx.arc(side/2 + margin,side/2 + margin, r, 0,2*Math.PI);
        //ctx.arc(x0+xoff, y0+yoff, _r, angle1, angle2);
        ctx.fillStyle= '#f55';
        ctx.fill();
        //style.marginTop = 72;
        // ui.toolselect.style.visibility = "hidden";
    }
    else{
        //ui.colorwheel.style.visibility = 'visible';
        var max_width = state.tool_width_max;
        var width = Math.max(1, Math.min(max_width, Math.round(zoomvalue * state.pen_width)));
        var params = {
            penWidth: width,
            penIndex: state.pen_index,
            penCount: state.pen_list.length,
            colors: [],
            opaqueColors: [],
            size: ui.colorwheel.width,
            borderColors: [],
            borderWidth: 4,
            startAngle : -Math.PI/2
        } 
        for(var i=0; i<state.pen_list.length; ++i){
            params.colors.push(SketchColor.getStateColor(state,i));
            params.opaqueColors.push(SketchColor.getStateColorOpaque(state,i));
            params.borderColors.push(SketchColor.getStateBorderColor(state,i)); }
        var penwidth = Math.round(pen.width);
        var style = ui.colorwheel.style;
        ui.colorwheel.innerHTML = "";
        params.drawBackground = !state.drawfore;
        drawPenControl(ui.colorwheel, params);
        style.backgroundColor = 'rgba(255,255,255,0.0)';
        //style.marginTop = 72;
        /*
        style.borderColor = SketchColor.getStateBorderColor(state);
        style.borderWidth = Math.max(2,(58 - 2*penwidth)/2) + 2*penwidth%2;
        style.marginTop = -Math.max(60, 2*penwidth) + 12;
        style.width = 2*penwidth+1;
        style.height = 2*penwidth+1;*/
        // style.visibility = "visible";
    }
    
    // enable/disable buttons
    setEnabled(ui.undo, state.undo.index > 0);
    setEnabled(ui.redo, state.undo.index < state.undo.history.length);
    setEnabled(ui.zoomout, true);
    setEnabled(ui.zoomin, true);
    //setEnabled(ui.zoomequal, state.zoomvalues[state.zoomindex] != 1 || state.default_zoom != 1);
}

})(exports);
