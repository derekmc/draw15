
// sketch.js
//
//   main entry point of Ghost Sketch application.
// global constants
//
//TODO check for older versions and port data from old format to new.
var STORAGE_PREFIX = "draw15_";
var SAVE_INTERVAL_TIME = 3000;  //this may be redundant, but why not?

var DRAG_NONE=0;
var DRAG_HORIZONTAL=1;
var DRAG_VERTICAL=2;

var MINDRAG = 5;
var LONG_PRESS_DELAY = 700;
var STATUS_TIMEOUT = 3000;

// uses these modules:
// Display2d
// LispMarkup
// Storage (chrome storage or shim)

// project modules
var Color = window.SketchColor;
var Vector = window.SketchVector;
var Geometry = window.SketchGeometry;
var Display = window.SketchDisplay;
var Buttons = window.SketchButtons;
var ToolInput = window.SketchToolInput;
var Animate = window.SketchAnimate;
var SaveLoad = window.SketchSaveLoad;
var Menu = window.SketchMenu;
var MenuActions = window.SketchMenuActions;
var Init = window.SketchInit;



// global for dispatching menu clicks.
var menuclick;

// main entry point for application.

var myScroll;
var test_globals = {}

window.addEventListener("load", main);
function main(){
    var ui = Init.init_ui();
    ui.canvas.width = window.innerWidth - 2;
    ui.canvas.height = window.innerHeight - 2;
    ui.foreground.width = ui.canvas.width;
    ui.foreground.height = ui.canvas.height;
    ui.background.width = ui.canvas.width;
    ui.background.height = ui.canvas.height;
    ui.buffer.width = ui.canvas.width;
    ui.buffer.height = ui.canvas.height;

    var store = window.Storage;
    var state = {};
    Init.init_state(state, ui);


    test_globals.state = state;
    test_globals.ui = ui;
    

    SaveLoad.loadState(state, store, stateLoadError, afterStateLoad);

    function stateLoadError(e){
        console.error(e.stack);
        console.error(e);
        Init.init_state(state, ui);
        afterStateLoad();
    }
    function afterStateLoad(){
        state.saveinterval = setInterval(SaveLoad.saveState, SAVE_INTERVAL_TIME, store, state);
            
        state.animateinterval = setInterval(function(){
            Animate.animate(state,ui)}, state.animate_delay);

        Display.refreshUI(ui, state);
        
        // a menu item was clicked
        menuclick = function(event, action, param){
            if(event) event.stopPropagation();
            var action_func = MenuActions[action];
            if(!action_func) return;
            action_func(ui,state, param,event); }
                
        function mouseAction(listener, event, action){
            listener.addEventListener(event, function(e){
                e.preventDefault();
                var x = e.pageX, y = e.pageY;
                action(ui,state, x,y); })}

        function longPressActions(listener, short_action, long_action, touch_start_action){
            var press_time = 0;
            var outstanding_press = false;
            var timeout_id = undefined;

            listener.addEventListener("mousedown", press_handler);
            document.addEventListener("mouseup", release_handler);
            // handle touch events too!
            listener.addEventListener("touchstart", press_handler);
            document.addEventListener("touchend", release_handler);

            function press_handler(e){
                if(!outstanding_press){ // prevent overlapping event handlers or multiple touches from messing this up.
                    e.stopPropagation();
                    e.preventDefault();
                    e.handled = true;
                    press_time = Date.now();
                    outstanding_press = true;
                    if(timeout_id !== undefined){
                        clearTimeout(timeout_id);
                        timeout_id = undefined; }
                    timeout_id = setTimeout(timeout_handler, LONG_PRESS_DELAY);
                    var x = e.pageX;
                    var y = e.pageY;
                    if(e.targetTouches && e.targetTouches.length){
                        var touch = e.targetTouches[0]; 
                        x = touch.pageX;
                        y = touch.pageY; }
                    if(touch_start_action){
                        touch_start_action(ui,state, x,y); }}}

            function release_handler(e){
                if(outstanding_press){
                    outstanding_press = false;
                    e.stopPropagation();
                    e.preventDefault();
                    e.handled = true;
                    var delay = Date.now() - press_time; 
                    if(timeout_id !== undefined){
                        clearTimeout(timeout_id);
                        timeout_id = undefined; }
                    if(delay < LONG_PRESS_DELAY){
                        if(short_action) short_action(ui,state); }
                    else{
                        console.warn("longpress was not triggered by timeout_handler, but release_handler;");
                        if(long_action) long_action(ui,state); }}}

            function timeout_handler(){
                if(outstanding_press){
                    outstanding_press = false;
                    if(long_action) long_action(ui,state); }}
        }


        function touchAction(listener, event, action){
            listener.addEventListener(event, function(e){
                if (e.targetTouches) {
                    e.preventDefault();
                    if(e.targetTouches.length){
                        var touch = e.targetTouches[0]; 
                        var x = touch.pageX,
                            y = touch.pageY;
                        action(ui,state, x,y); }}})}

        mouseAction(ui.canvas, "mousedown", ToolInput.mouseDownCanvas);
        mouseAction(ui.colorwheel, "mousedown", ToolInput.colorWheelDown);
        //mouseAction(ui.joystick, "mousedown", ToolInput.joystickDown);
        mouseAction(document, "mousemove", ToolInput.mouseMove);
        mouseAction(document, "mouseup", ToolInput.mouseUp);


        touchAction(ui.canvas, "touchstart", ToolInput.mouseDownCanvas);
        touchAction(ui.joystick, "touchstart", ToolInput.joystickDown);
        touchAction(document, "touchmove", ToolInput.mouseMove);
        document.addEventListener("touchend", function(e){ToolInput.mouseUp(ui,state)});
        //
        longPressActions(ui.colorwheel, ToolInput.mouseUp, ToolInput.colorWheelLongpress, ToolInput.colorWheelDown);
        longPressActions(ui.joystick, ToolInput.mouseUp, ToolInput.joystickLongpress, ToolInput.joystickDown);
        //touchAction(ui.colorwheel, "touchstart", ToolInput.colorWheelDown);
        
        // buttons
        touchAction(ui.erasebutton, 'touchstart', Buttons.eraseclick);
        touchAction(ui.linebutton, 'touchstart', Buttons.lineclick);
        touchAction(ui.undo, 'touchstart', Buttons.undoclick);
        touchAction(ui.redo, 'touchstart', Buttons.redoclick);
        touchAction(ui.menulink, 'touchstart', Buttons.menulink_click);
        touchAction(ui.banner_button, 'touchstart', Buttons.banner_click);
        touchAction(ui.zoomin, 'touchstart', Buttons.zoominclick);
        touchAction(ui.zoomout, 'touchstart', Buttons.zoomoutclick);
        touchAction(ui.banner_close_button, 'touchstart', Buttons.banner_close_click);
        
        mouseAction(ui.erasebutton, 'mousedown', Buttons.eraseclick);
        mouseAction(ui.linebutton, 'mousedown', Buttons.lineclick);
        mouseAction(ui.undo, 'mousedown', Buttons.undoclick);
        mouseAction(ui.redo, 'mousedown', Buttons.redoclick);
        mouseAction(ui.menulink, 'mousedown', Buttons.menulink_click);
        mouseAction(ui.banner_button, 'mousedown', Buttons.banner_click);
        mouseAction(ui.zoomin, 'mousedown', Buttons.zoominclick);
        mouseAction(ui.zoomout, 'mousedown', Buttons.zoomoutclick);
        mouseAction(ui.banner_close_button, 'mousedown', Buttons.banner_close_click);

        longPressActions(ui.toolcycle, Buttons.toolcycleclick, Buttons.toolcycle_longpress);
        //longPressActions(ui.zoomout, Buttons.zoomoutclick, Buttons.zoomout_longpress); 
        //longPressActions(ui.zoomin, Buttons.zoominclick, Buttons.zoomin_longpress); 

        
        // resize
        window.addEventListener("resize", function(e){
            var oldw = state.width;
            var oldh = state.height;
            var w = window.innerWidth - 2;
            var h = window.innerHeight - 2;
            var zoom_window = SketchGeometry.zoomWindowBounds(state);
            
            ui.canvas.width = window.innerWidth - 2;
            ui.canvas.height = window.innerHeight - 2;
            ui.foreground.width = ui.canvas.width;
            ui.foreground.height = ui.canvas.height;
            ui.background.width = ui.canvas.width;
            ui.background.height = ui.canvas.height;
            ui.buffer.width = ui.canvas.width;
            ui.buffer.height = ui.canvas.height;

            state.width = ui.canvas.width;
            state.height = ui.canvas.height;
            state.refresh = true;
            var focus_x = canvas.width * state.scene_display_focus_ratio[0];
            var focus_y = canvas.height * state.scene_display_focus_ratio[1];
            var focus = [focus_x, focus_y];
            state.scene_display.setDisplayDim([canvas.width, canvas.height]);
            state.scene_display.setDisplayFocus(focus);
            state.zoom_display.setDisplayDim([canvas.width, canvas.height]);
            state.zoom_display.setDisplayFocus(focus);
            Geometry.fitZoomBox(state,false);
        });
        state.refresh = true;
    }
}
