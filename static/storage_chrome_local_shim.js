(function(exports){
	exports.get = function(k, next){
	    if(typeof k === "string"){
			result = {};
			result[k] = JSON.parse(localStorage.getItem(k));
		    if(next) next(result); }
		else if(Array.isArray(k)){
			result = {};
			for(var i = 0; i<k.length; ++i){
		        result[k[i]] = JSON.parse(localStorage.getItem(k[i])); }
			if(next) next(result); }
		else if(typeof k === "object"){
			var result = {}
			for(var i in k){
		        result[i] = JSON.parse(localStorage.getItem(i)); }
			if(next) next(result); }
		else{
		    k = k.toString();
		    if(next) next({k: JSON.parse(localStorage.getItem(k))}); }
	}
	exports.set = function(items, next){
		if(typeof items === "object"){
			var result = {}
			for(var k in items){
		        localStorage.setItem(k, JSON.stringify(items[k])); }
			if(next) next(); }
		else{
			console.warn("storage_local_chrome_shim.js set: unacceptable paramter type for 'items'", items);
		}
	}
    exports.remove = function(k, next){
        if(Array.isArray(k)){
            for(var i=0; i<k.length; ++i){
                localStorage.removeItem(k[i]); }
        }
        else{
            localStorage.removeItem(k);
        }
        if(next) next();
    }
})(typeof exports === 'undefined'? this['Storage']={}: exports);
