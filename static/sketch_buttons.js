var exports = window.SketchButtons = {};

(function(exports){

exports.banner_click = banner_click;
exports.undoclick = undoclick;
exports.redoclick = redoclick;
exports.eraseclick = eraseclick;
exports.lineclick = lineclick;
exports.zoominclick = zoominclick;
exports.zoomoutclick = zoomoutclick;
exports.zoomin_longpress = zoomin_longpress;
exports.zoomout_longpress = zoomout_longpress;
exports.toolcycleclick = toolcycleclick;
exports.toolcycle_longpress = toolcycle_longpress;
exports.menulink_click = menulink_click;
exports.banner_close_click = banner_close_click;

var refreshAll = SketchDisplay.refreshAll;
var refreshUI = SketchDisplay.refreshUI;
var fitZoomBox = SketchGeometry.fitZoomBox;
var SCENEZOOM_INITIAL_RATIO = 8;

function banner_click(ui,state){
    if(state.show_scene){
        enable_scene(ui, state); }
    else{
        disable_scene(ui, state); }
	SketchDisplay.refreshAll(ui, state);
}

// ui element actions
function undoclick(ui,state){
    if(state.undo.index > 0){
		--state.undo.index;
		var undo_obj = state.undo.history[state.undo.index];
		var action = undo_obj[0],
		    target = undo_obj[1],
			data = undo_obj[2];
		if(action == "newline"){
            state.drawing.strokes.splice(target,1);
		}
		else if(action == "removeline"){
            state.drawing.strokes.splice(target,0,data);
		}
        refreshAll(ui, state);
    }
}
function redoclick(ui, state){
	if(state.undo.index < state.undo.history.length){
		var undo_obj = state.undo.history[state.undo.index];
		++state.undo.index;
		var action = undo_obj[0];
        for(var i=1; i < undo_obj.length - 1; i += 2){
            var target = undo_obj[i];
            var data = undo_obj[i + 1];
            if(action == "newline"){
                state.drawing.strokes.splice(target,0,data); }
            else if(action == "removeline"){
                state.drawing.strokes.splice(target,1); }
            else if(action == "cutline"){
                // cuts remove a stroke, then add one or two more.
                if(i%6 == 1){
                    state.drawing.strokes.splice(target,1); }
                else if(i%6 == 3){
                    state.drawing.strokes.splice(target,0,data); }
                else if(i%6 == 5){
                    state.drawing.strokes.splice(target,0,data); }
                else{
                    throw new Error("SketchButtons.redoclick: undo_obj index should not be odd."); }
            }
        }
	}
}
function zoominclick(ui,state){
    var scale = state.zoom_display.getScale();
    var scene_scale = state.scene_display.getScale();
    var s = state.ZOOM_FACTOR;
    var new_scale = scale;
    var new_scene_scale = scene_scale;
    if(state.show_scene){
        ++state.scene_ratio_index;
        if(state.scene_ratio_index == state.scene_ratios.length){
            state.scene_ratio_index = 0; 
            new_scene_scale = Math.pow(2, Math.round(Math.log2(s * scene_scale)));
            if(new_scene_scale - state.ZOOM_MAX > 0.000001){
                new_scene_scale = state.ZOOM_MAX;
                state.scene_ratio_index = state.scene_ratios.length - 1; }
            state.scene_display.setScale(new_scene_scale); }
        // console.log("scene_ratio", state.scene_ratios[state.scene_ratio_index], state.scene_ratios, state.scene_ratio_index);
        state.zoom_display.setScale(new_scene_scale * state.scene_ratios[state.scene_ratio_index]); }
    else{
        var new_scale = Math.pow(2, Math.round(Math.log2(s * scale)));
        if(new_scale / state.ZOOM_MAX < 1.0001){
            state.zoom_display.setScale(new_scale); }
        else{
            state.zoom_display.setScale(state.ZOOM_MAX); }}
    state.status = ""; 
    refreshAll(ui, state);
}
function zoomoutclick(ui,state){
    var scale = state.zoom_display.getScale();
    var scene_scale = state.scene_display.getScale();
    var s = 1 / state.ZOOM_FACTOR;
    var new_scene_scale = scene_scale;
    if(state.show_scene){
        --state.scene_ratio_index;
        if(state.scene_ratio_index < 0){
            state.scene_ratio_index += state.scene_ratios.length; 
            new_scene_scale = Math.pow(2, Math.round(Math.log2(s * scene_scale)));
            if(new_scene_scale / state.ZOOM_MIN < 0.999999){
                new_scene_scale = state.ZOOM_MIN;
                state.scene_ratio_index = 0; }
            state.scene_display.setScale(new_scene_scale); }
        state.zoom_display.setScale(new_scene_scale * state.scene_ratios[state.scene_ratio_index]); }
    else{
        var new_scale = Math.pow(2, Math.round(Math.log2(s * scale)));
        if(new_scale / state.ZOOM_MIN > 0.9999){
            state.zoom_display.setScale(new_scale); }
        else{
            state.zoom_display.setScale(state.ZOOM_MIN); }}
    state.status = "";
    refreshAll(ui, state);
}

function zoomin_longpress(ui,state){
    enable_scene(ui, state);
}

function disable_scene(ui,state){
    var scene_scale = state.scene_display.getScale();
    var scale = state.zoom_display.getScale();
    state.show_scene = !state.show_scene;
    state.status = "";
    var w = ui.canvas.width;
    var h = ui.canvas.height;
    var focus_x = w * state.scene_display_focus_ratio[0];
    var focus_y = h * state.scene_display_focus_ratio[1];
    if(state.show_scene){
        state.scene_display.setDisplayFocus([focus_x, focus_y]);
        state.scene_fade_start = Date.now();
        state.scene_ratio_index = 0;
        var new_scale = scale * state.scene_ratio;
        state.scene_display.setScale(scale);
        state.zoom_display.setScale(new_scale);
        state.scene_display.setSpaceFocus(state.zoom_display.getSpaceFocus());
        fitZoomBox(state, true);
        //scene_banner(ui,state);
    }
    else if(scale - state.ZOOM_MAX > -0.000001){
        state.scene_display.setDisplayFocus([w/2, h/2]);
        state.zoom_display.setDisplayFocus([w/2, h/2]);
        state.zoom_display.setSpaceFocus(state.scene_display.getSpaceFocus());
        state.zoom_display.setScale(state.ZOOM_MAX); }
    refreshAll(ui, state);
}

function zoomout_longpress(ui,state){
    disable_scene(ui, state);
}
function enable_scene(ui,state){
    var scale = state.zoom_display.getScale();
    state.show_scene = !state.show_scene;
    state.status = "";
    var w = ui.canvas.width;
    var h = ui.canvas.height;
    if(state.show_scene){
        state.scene_ratio_index = 0;
        var scene_ratio = state.scene_ratios[state.scene_ratio_index];
        if(scale/scene_ratio - state.ZOOM_MIN < 0.000001){
            scale = state.ZOOM_MIN * scene_ratio;
            state.zoom_display.setScale(scale); }
        var focus_x = w * state.scene_display_focus_ratio[0];
        var focus_y = h * state.scene_display_focus_ratio[1];
        var focus = [focus_x, focus_y];
        state.scene_ratio_index = 0;
        state.scene_display.setDisplayFocus([focus_x, focus_y]);
        state.scene_fade_start = Date.now();
        var new_scale = scale / state.scene_ratio;
        if(new_scale < state.ZOOM_MIN){
            new_scale = state.ZOOM_MIN; }
        state.scene_display.setSpaceFocus(state.zoom_display.getSpaceFocus());
        state.scene_display.setScale(new_scale);
        state.zoom_display.setScale(scale);
        state.zoom_display.setDisplayFocus(focus);
    }
    else{
        state.scene_display.setDisplayFocus([w/2, h/2]);
        state.zoom_display.setDisplayFocus([w/2, h/2]);
        state.zoom_display.setSpaceFocus(state.scene_display.getSpaceFocus());
        state.zoom_display.setScale(state.scene_display.getScale());
        fitZoomBox(state, true); }

    refreshAll(ui, state);
}

function lineclick(ui,state){
    state.draw_line_enabled = !state.draw_line_enabled;
    refreshUI(ui, state);
}

function eraseclick(ui,state){
    state.erase = !state.erase;
    refreshUI(ui, state);
}

function toolcycleclick(ui,state){
    if(state.erase){
        state.erase = false; }
    else{
        state.pencil_enabled = !state.pencil_enabled;
        state.pen_list[state.pen_index].color[3] = state.pencil_enabled? state.pencil_opacity : 1.0;
    }
    refreshUI(ui, state);
}
function toolcycle_longpress(ui,state){
    state.drawfore = !state.drawfore;
    state.status = (state.drawfore? "Foreground " : "Background");
    state.status_expire = Date.now() + STATUS_TIMEOUT;
    refreshAll(ui, state);
}
function menulink_click(ui,state){
    Menu.render(ui.overlay, "main",state);
    state.menu_stack.push("main");
    ui.overlay.style.visibility = "visible";
}
function banner_close_click(ui,state){
    ui.banner.style.visibility = 'hidden';
    if(state.hasOwnProperty('banner_close_action') &&
       state.banner_close_action){
        state.banner_close_action(ui,state);
    }
}

})(exports);
