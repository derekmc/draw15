

var exports = window.SketchMenuActions = {};

(function(exports){

var Menu = window.SketchMenu;
exports.navigate = navigate;
exports.browser_navigate = browser_navigate;
exports.backnav = backnav;
exports.back = back;
exports.confirm_yes = confirm_yes;
exports.prompt_done = prompt_done;
exports.confirm_no = confirm_no;
exports.toggle_pencil = toggle_pencil;
exports.pen_index = pen_index;
exports.pen_color = pen_color;
exports.pencil_opacity = pencil_opacity;
exports.pen_brightness = pen_brightness;
exports.pen_drawfore = pen_drawfore;
exports.pen_drawline = pen_drawline;
exports.page_scene_draw = page_scene_draw;
exports.page_clear = page_clear;
exports.page_view = page_view;
exports.page_home = page_home;
exports.move_scene_window = move_scene_window;
exports.manual = manual;
exports.save = save;
exports.load = load;


function move_scene_window(ui,state){
    state.menu_stack.length = 0;
    console.log("TODO move_scene_window");
}


function save(ui,state){
    state.menu_stack.push("prompt_save_file");
    Menu.render(ui.overlay, "prompt",
                {message: "Save As"});
    state.prompt_action = function(state,ui, value,e){
        //alert("TODO: save to '" + value + "'");
        state.filename = value;
        SaveLoad.saveFile(state, value);
        return true;
    }
    document.getElementById("prompt_input").value = state.filename;
}
function load(ui,state){
    state.menu_stack.push("prompt_load_file");
    Menu.render(ui.overlay, "choose_file",
                {message: "Load File"});
    state.prompt_action = function(state,ui, value,e){
        //alert("TODO: load from'" + value + "'");
        SaveLoad.loadFile(state, e.target.files[0], null, after);
        if(value){
            var parts = value.split(/[\\\/]/);
            state.filename = parts[parts.length-1];
            return true; }
        function after(){
            SketchGeometry.fitZoomBox(state);
            SketchDisplay.refreshAll(ui, state);
        }
    }
}

function navigate(ui,state, param){
    if(!param || typeof param !== "string" || param.length == 0){
        return; }
    var menu_name = param;
    state.menu_stack.push(menu_name);
    Menu.render(ui.overlay, menu_name,state);
}
function browser_navigate(ui,state, param){
    window.location.href = param;
}

function backnav(ui,state, param){
    state.menu_stack.pop();
    navigate(ui,state, param);
}

function back(ui,state){
    state.menu_stack.pop();
    window.onscroll = null;
    var menu_name = state.menu_stack[state.menu_stack.length - 1];
    if(menu_name){
        Menu.render(ui.overlay, menu_name,state); }
    else{
        Menu.render(ui.overlay, ""); }
}

function confirm_yes(ui,state){
    if(typeof state.confirm_action == "function"){
        state.confirm_action(state,ui); }
    delete state["confirm_action"];
    state.menu_stack.pop();
    if(!state.menu_stack.length){
        Menu.render(ui.overlay, ""); }
    else{
        Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state); }
}

function confirm_no(ui,state){
    delete state["confirm_action"];
    state.menu_stack.pop();
    if(!state.menu_stack.length){
        Menu.render(ui.overlay, ""); }
    else{
        Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state); }
}

function prompt_done(ui,state, param,e){
    if(typeof state.prompt_action == "function"){
        var value = document.getElementById("prompt_input").value;
        if(!state.prompt_action(state,ui, value,e)){
            return; }
        state.menu_stack.pop();
        if(!state.menu_stack.length){
            Menu.render(ui.overlay, ""); }
        else{
            Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state); }
    }
}

function pen_brightness(ui,state){
    state.menu_stack.push("prompt_pen_brightness");
    var color = state.pen_list[state.pen_index].color;
    var pen = state.pen_list[state.pen_index];
    var brightness = pen.brightness;
    var max = state.pen_brightness_max;
    var text = "" + Math.round(max * brightness);
    state.prompt_action = function(state,ui, value){
        var n = parseInt(value);
        var valid = !isNaN(n) && 0 <= n && n <= max;
        if(valid){
            pen.brightness = n/max;
            color[3] = 1.0;
            return true; }
        else{
            document.getElementById("prompt_input").value = text;
            state.menu_stack.pop();
            menuclick(null, 'pen_brightness');
            return false; }}
    Menu.render(ui.overlay, "prompt",
                {message: "Color Brightness (0-" + max + ")"});
    document.getElementById("prompt_input").value = text;
}
function pencil_opacity(ui,state){
    state.menu_stack.push("prompt_pencil_opacity");
    var color = state.pen_list[state.pen_index].color;
    var opacity = state.pencil_opacity;
    var max = state.pencil_opacity_max;
    var text = "" + Math.round(max * opacity);
    state.prompt_action = function(state,ui, value){
        var n = parseInt(value);
        var valid = !isNaN(n) && 1 <= n && n < max;
        if(valid){
            state.pencil_opacity = n/max;
            color[3] = state.pencil_opacity;
            return true; }
        else{
            document.getElementById("prompt_input").value = text;
            state.menu_stack.pop();
            menuclick(null, 'pencil_opacity');
            return false; }}
    Menu.render(ui.overlay, "prompt",
                {message: "Pencil Opacity (1-" + (max - 1) + ")"});
    document.getElementById("prompt_input").value = text;
}


function pen_color(ui,state){
    state.menu_stack.push("prompt_pen_color");
    var color = state.pen_list[state.pen_index].color;
    var text = (color[0] < 16? "0": "") + color[0].toString(16) + 
               (color[1] < 16? "0": "") + color[1].toString(16) + 
               (color[2] < 16? "0": "") + color[2].toString(16);
    state.prompt_action = function(state,ui, value){
        var hexcolor_regex = /^([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])$/
        var match = value.match(hexcolor_regex);
        if(match){
            var pen = state.pen_list[state.pen_index];
            if(value == "000000") pen.brightness = 0;
            else if(value == "ffffff") pen.brightness = 1;
            else pen.brightness = 0.5;
            var color = pen.color;
            color[0] = parseInt(match[1],16);
            color[1] = parseInt(match[2],16);
            color[2] = parseInt(match[3],16);
            SketchDisplay.refreshUI(ui, state);
            return true; }
        else{
            document.getElementById("prompt_input").value = text;
            state.menu_stack.pop();
            menuclick(null, 'pen_color');
            return false; }}
    Menu.render(ui.overlay, "prompt",
                {message: " Hex Color #" + (state.pen_index+1)});
    document.getElementById("prompt_input").value = text;

}

function pen_index(ui,state){
    state.pen_index = (state.pen_index + 1) % state.pen_list.length;
    SketchDisplay.refreshUI(ui, state);
    Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state);
}

function pen_drawline(ui,state){
    state.draw_line_enabled = !state.draw_line_enabled;
    Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state);
}
function pen_drawfore(ui,state){
    state.drawfore = !state.drawfore;
    Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state);
}
function toggle_pencil(ui,state){
    // duplicated in SketchToolInput
    state.pencil_enabled = !state.pencil_enabled;
    state.pen_list[state.pen_index].color[3] = (state.pencil_enabled? state.pencil_opacity : 1.0);
    state.status = (state.pencil_enabled? "Pencil" : "Pen") + " Mode";
    state.status_expire = Date.now() + STATUS_TIMEOUT;
    SketchDisplay.refreshUI(ui, state);
    Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state);
}

function next_scene_setting(ui,state){
    if(state.show_scene){
        SketchButtons.zoomin_longpress(ui, state); }
    else{
        SketchButtons.zoomout_longpress(ui, state); }
    //SketchDisplay.refreshAll(ui, state);
    Menu.render(ui.overlay, state.menu_stack[state.menu_stack.length - 1], state);
}

function page_clear(ui,state){
    state.menu_stack.push("confirm_delete_page");
    state.confirm_action = function(state,ui){
        state.drawing.clear();
        state.undo.history = [];
        state.undo.index = 0;
        SketchDisplay.refreshAll(ui, state); }
    Menu.render(ui.overlay, "confirm",
                {message: "Clear Page?"});
}

function page_home(ui,state){
    state.menu_stack.length = 0;
    Menu.render(ui.overlay, "");
    state.zoom_display.setSpaceFocus([0,0]);
    SketchGeometry.fitZoomBox(state);
    SketchDisplay.refreshAll(ui, state);
}

function page_view(ui,state){
    state.menu_stack.length = 0;
    Menu.render(ui.overlay, "");
    state.hide_ui = true;
    SketchDisplay.refreshAll(ui, state);
}

function manual(ui,state){
    state.menu_stack.push("manual");
    Menu.loadpage(ui.overlay, "./manual.html");
}


})(exports);
